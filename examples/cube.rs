#![allow(mixed_script_confusables)]

use itertools::Itertools;
use playgraphics::d3::Point3;
use playgraphics::d3::Projection;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point as Point2;
use playgraphics::System;
use playgraphics::Vector;
use std::f64::consts::PI as π;

const DIM: usize = 800;

fn main() {
    let mut lines = {
        let mut lines = Vec::new();
        for (&a, &b, &c) in itertools::cons_tuples(
            [-1., 1.]
                .iter()
                .cartesian_product([-1., 1.].iter())
                .cartesian_product([-1., 1.].iter()),
        ) {
            lines.push((Point3::new(a, b, c), Point3::new(-a, b, c)));
            lines.push((Point3::new(a, b, c), Point3::new(a, -b, c)));
            lines.push((Point3::new(a, b, c), Point3::new(a, b, -c)));
        }
        lines
    };

    const PROJ: Projection = Projection {
        distance: 3.,
        fov: π / 2.,
    };
    const DEG: f64 = (0.5 / 360.) * 2. * π;

    let mut sys = System::new("Wireframe Cube", DIM, DIM);
    loop {
        sys.draw_and_render(|g| {
            g.transform = Affine::dilation(Point2::origin(), DIM as f64 / 4.)
                * Affine::translation(Vector::new(2., 2.));
            g.color = Color::black();
            g.clear();
            g.color = Color::green();
            for &(p, q) in &lines {
                g.draw_line(PROJ.project(p), PROJ.project(q));
            }
        });
        for (p, q) in &mut lines {
            *p = p.rotate_y(DEG);
            *q = q.rotate_y(DEG);
        }
    }
}
