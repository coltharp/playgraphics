#![allow(mixed_script_confusables)]

use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;
use rand::prelude::*;
use rayon::prelude::*;

use std::f64::consts::PI as π;
const O: Point = Point::origin();
const DIM: usize = 800;
const NUM_POINTS: usize = 32;
const NUM_SKIP: usize = 32;
const NUM_SAMPLES: usize = 8192;

fn main() {
    let letter_width = 4.;
    let letter_height = 4.;
    let n_rules: [Affine; 4] = [
        // left
        [
            Affine::dilation_x(O, 4. / letter_width),
            Affine::dilation_y(O, 1. / letter_height),
            Affine::rotation(Point::new(0.5, 0.5), π / 2.),
        ]
        .iter()
        .rev()
        .product(),
        // middle up
        [
            Affine::dilation_x(O, 1. / letter_width),
            Affine::dilation_y(O, 1. / letter_height),
            Affine::translation(Vector::new(1., 2.)),
        ]
        .iter()
        .rev()
        .product(),
        // middle down
        [
            Affine::dilation_x(O, 1. / letter_width),
            Affine::dilation_y(O, 1. / letter_height),
            Affine::translation(Vector::new(2., 1.)),
        ]
        .iter()
        .rev()
        .product(),
        // right
        [
            Affine::dilation_x(O, 4. / letter_width),
            Affine::dilation_y(O, 1. / letter_height),
            Affine::rotation(Point::new(0.5, 0.5), π / 2.),
            Affine::translation_x(3.),
        ]
        .iter()
        .rev()
        .product(),
    ]
    .map(|t: Affine| Affine::translation_x(5.) * t);
    let g_rules: [Affine; 4] = [
        // top
        [
            Affine::dilation_x(O, 4. / letter_width),
            Affine::dilation_y(O, 1. / letter_height),
            Affine::rotation(Point::new(2., 0.5), π),
            Affine::translation_y(3.),
        ]
        .iter()
        .rev()
        .product(),
        // left
        [
            Affine::dilation_x(O, 3. / letter_width),
            Affine::dilation_y(O, 1. / letter_height),
            Affine::rotation(Point::new(0., 0.), -π / 2.),
            Affine::translation_y(3.),
        ]
        .iter()
        .rev()
        .product(),
        // bottom
        [
            Affine::dilation_x(O, 3. / letter_width),
            Affine::dilation_y(O, 1. / letter_height),
            Affine::translation_x(1.0),
        ]
        .iter()
        .rev()
        .product(),
        // right
        [
            Affine::dilation_x(O, 1. / letter_width),
            Affine::dilation_y(O, 1. / letter_height),
            Affine::rotation(Point::new(0.5, 0.5), π / 2.),
            Affine::translation(Vector::new(3., 1.)),
        ]
        .iter()
        .rev()
        .product(),
    ]
    .map(|t: Affine| t * Affine::translation_x(-5.));

    let distr_left_x = rand::distributions::Uniform::new_inclusive(0., 4.);
    let distr_left_y = rand::distributions::Uniform::new_inclusive(0., 1.);
    let distr_right_x = rand::distributions::Uniform::new_inclusive(5., 9.);
    let distr_right_y = rand::distributions::Uniform::new_inclusive(0., 1.);
    let points = (0..NUM_POINTS)
        .into_par_iter()
        .flat_map_iter(|_| {
            let mut rng = thread_rng();
            let p = if rng.gen() {
                Point::new(distr_left_x.sample(&mut rng), distr_left_y.sample(&mut rng))
            } else {
                Point::new(
                    distr_right_x.sample(&mut rng),
                    distr_right_y.sample(&mut rng),
                )
            };
            itertools::unfold(p, move |p| {
                let t = if p.x < 4.5 {
                    n_rules.choose(&mut rng)
                } else {
                    g_rules.choose(&mut rng)
                }
                .unwrap();
                *p = t * *p;
                Some(*p)
            })
            .skip(NUM_SKIP)
            .take(NUM_SAMPLES)
        })
        .collect::<Vec<_>>();

    let mut sys = System::new("Dual IFS Test", DIM, DIM);
    sys.draw_and_render(|g| {
        g.transform = Affine::dilation(O, DIM as f64 / 9.);
        g.color = Color::white();
        g.clear();
        g.color = Color::black();
        for p in points {
            g.draw_point(p);
        }
    });
    sys.park();
}
