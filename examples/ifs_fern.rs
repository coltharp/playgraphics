use itertools::Itertools;
use ndarray::prelude::*;
use playgraphics::ifs;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use rand::prelude::*;
use rand_distr::weighted_alias::WeightedAliasIndex;
use rayon::prelude::*;

const O: Point = Point::origin();

const NUM_POINTS: usize = 2_usize.pow(6);
const NUM_SKIP: usize = 2_usize.pow(5);
const NUM_TAKE: usize = 2_usize.pow(15);
const DIM: usize = 800;

fn main() {
    let rules: [Affine; 4] = [
        // stem
        [Affine::dilation_x(O, 0.), Affine::dilation_y(O, 0.16)]
            .iter()
            .rev()
            .product(),
        // first leaf
        [
            Affine::reflection_x(0.),
            Affine::dilation(O, (4_f64.powi(2) + 3_f64.powi(2)).sqrt() / 16.),
            Affine::rotation(O, (-4_f64).atan2(3.)),
            Affine::translation_y(0.045),
        ]
        .iter()
        .rev()
        .product(),
        // second leaf
        [
            Affine::dilation(O, (4.5_f64.powi(2) + 3.5_f64.powi(2)).sqrt() / 16.),
            Affine::rotation(O, 4.5_f64.atan2(3.5)),
            Affine::translation_y(0.16),
        ]
        .iter()
        .rev()
        .product(),
        // rest
        [
            Affine::dilation(O, (0.5_f64.powi(2) + 13.5_f64.powi(2)).sqrt() / 16.),
            Affine::rotation(O, (-0.5_f64).atan2(13.5)),
            Affine::translation_y(0.16),
        ]
        .iter()
        .rev()
        .product(),
    ];
    let dist =
        WeightedAliasIndex::new(vec![1, 1, 1, 8])
            .unwrap()
            .map(|i| playgraphics::util::Carrier {
                value: i,
                function: &rules[i],
            });

    let inits = (0..NUM_POINTS)
        .into_par_iter()
        .map(|_| Point::new(random::<f64>() - 0.5, random()));
    let results = ifs::run_par(&dist, inits)
        .flat_map_iter(|it| it.skip(NUM_SKIP).take(NUM_TAKE))
        .collect::<Vec<_>>();

    let t = Affine::dilation(O, DIM as f64) * Affine::translation_x(0.5);
    let mut buckets = Array3::<usize>::zeros((DIM, DIM, rules.len()));
    for (c, p) in results {
        let p = t * p;
        if p.x < 0. || p.y < 0. {
            continue;
        }
        let x = p.x.round() as usize;
        let y = p.y.round() as usize;
        if x >= DIM || y >= DIM {
            continue;
        }
        buckets[(x, y, c.value)] += 1;
    }
    let colors = (0..4)
        .flat_map(|_| Color::new(random(), random(), random()))
        .collect_vec();

    let mut sys = System::new("IFS Fern", DIM, DIM);

    loop {
        sys.draw_and_render(|g| {
            g.color = Color::white();
            g.clear();
            for i in 0..DIM {
                for j in 0..DIM {
                    let b = buckets.index_axis(Axis(0), i);
                    let b = b.index_axis(Axis(0), j);
                    let total = b.iter().sum::<usize>() as f64;
                    g.color = if total == 0. {
                        Color::white()
                    } else {
                        let red = b
                            .iter()
                            .enumerate()
                            .map(|(i, &n)| n as f64 * colors[i].r())
                            .sum::<f64>()
                            / total;
                        let green = b
                            .iter()
                            .enumerate()
                            .map(|(i, &n)| n as f64 * colors[i].g())
                            .sum::<f64>()
                            / total;
                        let blue = b
                            .iter()
                            .enumerate()
                            .map(|(i, &n)| n as f64 * colors[i].b())
                            .sum::<f64>()
                            / total;
                        Color::new(red, green, blue).unwrap()
                    };
                    g.draw_point(Point::new(i as f64, j as f64));
                }
            }
        });
    }
}
