use playgraphics::Color;
use playgraphics::MouseButton;
use playgraphics::System;

fn main() {
    let mut sys = System::new("Line", 800, 800);
    sys.draw_and_render(|g| {
        g.color = Color::white();
        g.clear()
    });
    let p = sys.wait_click(MouseButton::Left);
    loop {
        let q = sys.mouse_pos();
        sys.draw_and_render(|g| {
            g.color = Color::white();
            g.clear();
            g.color = Color::black();
            g.draw_line(p, q);
        });
    }
}
