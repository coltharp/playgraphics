use playgraphics::Color;
use playgraphics::Event;
use playgraphics::MouseButton;
use playgraphics::System;

fn main() {
    let mut sys = System::new("Fill polygon", 800, 800);
    let mut points = Vec::new();
    points.push(sys.mouse_pos());
    loop {
        *points.last_mut().unwrap() = sys.mouse_pos();
        let events = sys.draw_and_render(|g| {
            g.color = Color::white();
            g.clear();
            g.color = Color::black();
            g.fill_polygon(&points);
        });
        if events.contains(&Event::MouseDown(MouseButton::Left)) {
            points.push(sys.mouse_pos());
        }
    }
}
