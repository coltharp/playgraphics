use playgraphics::ifs;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;
use rayon::prelude::*;

const NUM_POINTS: usize = 2_usize.pow(6);
const NUM_ITERATIONS: usize = 2_usize.pow(12);
const DIM: usize = 800;

fn main() {
    let rules = [
        Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(1. / 3., 0.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(1. / 3., 2. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(2. / 3., 0.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(2. / 3., 1. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(0., 1. / 3.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(0., 2. / 3.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(2. / 3., 2. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
    ];

    let mut sys = System::new("Buzzing Carpet", DIM, DIM);
    let t = Affine::dilation(Point::origin(), DIM as f64);
    loop {
        let inits = (0..NUM_POINTS)
            .into_par_iter()
            .map(|_| Point::new(rand::random(), rand::random()));
        let points = ifs::run_slice_par(&rules, inits)
            .flat_map_iter(|it| it.take(NUM_ITERATIONS))
            .map(|u| u.1)
            .collect::<Vec<_>>();
        sys.draw_and_render(|g| {
            g.color = Color::white();
            g.clear();
            g.color = Color::black();
            for p in points {
                g.draw_point(t * p);
            }
        });
    }
}
