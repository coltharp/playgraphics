use itertools::Itertools;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::MouseButton;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;

#[derive(Copy, Clone)]
enum Symbol<T, NT> {
    T(T),
    NT(NT),
}
use Symbol::*;

trait Grammar {
    type Terminal: Copy;
    type Nonterminal: Copy;

    fn expand_one(nt: Self::Nonterminal) -> Vec<Symbol<Self::Terminal, Self::Nonterminal>>;
    fn extend(
        sym: Symbol<Self::Terminal, Self::Nonterminal>,
        out: &mut Vec<Symbol<Self::Terminal, Self::Nonterminal>>,
    ) {
        match sym {
            NT(nt) => out.extend(Self::expand_one(nt)),
            _ => out.push(sym),
        }
    }
    fn expand(
        str: &[Symbol<Self::Terminal, Self::Nonterminal>],
    ) -> Vec<Symbol<Self::Terminal, Self::Nonterminal>> {
        let mut ret = Vec::new();
        for &sym in str {
            Self::extend(sym, &mut ret);
        }
        ret
    }
}

struct KochGrammar;

#[derive(Copy, Clone)]
enum KochTerminal {
    Plus,
    Minus,
}

use KochTerminal::*;

#[derive(Copy, Clone)]
enum KochNonterminal {
    A,
    B,
    C,
}
use KochNonterminal::*;

impl Grammar for KochGrammar {
    type Terminal = KochTerminal;
    type Nonterminal = KochNonterminal;
    fn expand_one(nt: Self::Nonterminal) -> Vec<Symbol<Self::Terminal, Self::Nonterminal>> {
        match nt {
            A => vec![T(Plus), NT(A), T(Minus), NT(C), T(Minus), NT(A), T(Plus)],
            B => vec![NT(B), T(Plus), NT(C), T(Plus), NT(B)],
            C => vec![NT(A), T(Minus), NT(C), T(Minus), NT(A)],
        }
    }
}

impl KochGrammar {
    fn read_str(str: &str) -> Vec<Symbol<KochTerminal, KochNonterminal>> {
        let mut ret = Vec::new();
        for c in str.chars() {
            ret.push(match c {
                '+' => T(Plus),
                '-' => T(Minus),
                'A' => NT(A),
                'B' => NT(B),
                'C' => NT(C),
                _ => panic!(),
            });
        }
        ret
    }
}

fn exec(sys: &System, str: &[Symbol<KochTerminal, KochNonterminal>]) -> Vec<Point> {
    let mut p = Point { x: 0., y: 0. };
    let mut theta = 0.;
    let mut ret = vec![p];
    for s in str {
        match s {
            T(Plus) => theta += std::f64::consts::PI / 6.,
            T(Minus) => theta -= std::f64::consts::PI / 6.,
            _ => {
                let q = Affine::rotation(p, theta) * (p + Vector { x: 1., y: 0. });
                p = q;
            }
        }
        ret.push(p);
    }
    let t = playgraphics::util::auto_place(
        sys.width() as f64,
        sys.height() as f64,
        ret.iter().copied(),
    );
    for p in ret.iter_mut() {
        *p *= t;
    }
    ret
}

fn main() {
    let mut input = KochGrammar::read_str("B++++B++++B++++B++++B++++B");
    let mut sys = System::new("Basic turtle", 800, 800);
    for _ in 1.. {
        let points = exec(&sys, &input);
        sys.draw_and_render(|g| {
            g.color = Color::white();
            g.clear();
            g.color = Color::black();
            for (&p, &q) in points.iter().tuple_windows() {
                g.draw_line(p, q);
            }
        });
        sys.render();
        sys.wait_click(MouseButton::Left);
        input = KochGrammar::expand(&input);
    }
}
