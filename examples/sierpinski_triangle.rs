use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Graphics;
use playgraphics::MouseButton;
use playgraphics::Point;
use playgraphics::System;

fn draw_sierpinski_triangle(g: &mut Graphics, p: Point, q: Point, r: Point, limit: usize) {
    if limit == 0 {
        return;
    }
    g.draw_polygon(&[p, q, r]);
    let pq = p.midpoint(q);
    let qr = q.midpoint(r);
    let rp = r.midpoint(p);
    draw_sierpinski_triangle(g, p, pq, rp, limit - 1);
    draw_sierpinski_triangle(g, q, pq, qr, limit - 1);
    draw_sierpinski_triangle(g, r, qr, rp, limit - 1);
}

fn main() {
    let mut sys = System::new("Sierpinski triangle", 800, 800);
    sys.draw_and_render(|g| {
        g.color = Color::white();
        g.clear()
    });
    let p = sys.wait_click(MouseButton::Left);
    loop {
        let q = sys.mouse_pos();
        let r = Affine::rotation(q, std::f64::consts::PI / 3.) * p;
        sys.draw_and_render(|g| {
            g.color = Color::white();
            g.clear();
            g.color = Color::black();
            draw_sierpinski_triangle(g, p, q, r, 9);
        });
    }
}
