#![allow(mixed_script_confusables)]

use itertools::Itertools;
use playgraphics::d3::Point3;
use playgraphics::d3::Projection;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point as Point2;
use playgraphics::System;
use playgraphics::Vector;
use std::f64::consts::PI as π;

const DIM: usize = 800;

fn main() {
    let mut lines = {
        let buf = {
            const LEN: usize = 5;
            let mut buf = [(0., 0.); LEN];
            for (i, u) in buf.iter_mut().enumerate() {
                let a = i as f64 * 2. * π / (LEN - 1) as f64;
                *u = (a.cos(), a.sin());
            }
            buf
        };

        const Y: f64 = -1.;
        let mut lines = Vec::new();
        for ((x0, z0), (x1, z1)) in buf.iter().copied().tuple_windows() {
            lines.push((Point3::new(0., 2., 0.), Point3::new(x0, Y, z0)));
            lines.push((Point3::new(x0, Y, z0), Point3::new(x1, Y, z1)));
        }
        lines
    };

    const PROJ: Projection = Projection {
        distance: 3.,
        fov: π / 2.,
    };
    const DEG: f64 = (0.5 / 360.) * 2. * π;

    let mut sys = System::new("Wireframe Pyramid", DIM, DIM);
    loop {
        sys.draw_and_render(|g| {
            g.transform = Affine::dilation(Point2::origin(), (DIM / 4) as f64)
                * Affine::translation(Vector::new(2., 2.));
            g.color = Color::black();
            g.clear();
            g.color = Color::green();
            for &(p, q) in &lines {
                g.draw_line(PROJ.project(p), PROJ.project(q));
            }
        });
        for (p, q) in &mut lines {
            *p = p.rotate_y(DEG);
            *q = q.rotate_y(DEG);
        }
    }
}
