#![allow(mixed_script_confusables)]

use clap::Parser;
use ordered_float::NotNan;
use playgraphics::lsystem;
use playgraphics::lsystem::State;
use playgraphics::lsystem::Symbol;
use playgraphics::paint;
use playgraphics::util::Keyed;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Linear;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;
use rand::prelude::*;
use std::collections::BinaryHeap;
use std::f64::consts::PI as π;
use std::path::PathBuf;

/// Initial string;
const STRING: &str = "X";

/// Number of iterations for which to expand the L-system.
const ITERATIONS: usize = 7;

/// L-system rendering parameters.
const PARAMS: lsystem::Params = lsystem::Params {
    translation: Vector::new(1., 0.),
    angle: (25. / 360.) * 2. * π,
};

/// Initial drawing state;
const INIT: State = State {
    pos: Point::new(0., 0.),
    angle: π / 4.,
};

/// Viewport dimension.
const DIM: usize = 800;

/// "Lexical" syntax.
#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
enum Token {
    F,
    P,
    M,
    Open,
    Close,
    Special(char),
}

impl Token {
    /// Parse a stream of chars to a list of Syms.
    fn parse(input: &str) -> Result<Vec<Token>, Box<dyn std::error::Error>> {
        use combine::any;
        use combine::choice;
        use combine::eof;
        use combine::many;
        use combine::token;
        use combine::value;
        use combine::Parser;

        many(choice!(
            token('F').with(value(Token::F)),
            token('+').with(value(Token::P)),
            token('-').with(value(Token::M)),
            token('(').with(value(Token::Open)),
            token(')').with(value(Token::Close)),
            any().map(Token::Special)
        ))
        .skip(eof())
        .parse(input)
        .map(|u| u.0)
        .map_err(|e| e.into())
    }
}

impl Symbol for Token {
    fn expand(self) -> Vec<Self> {
        match self {
            Token::Special(c) => vec![
                Token::F,
                Token::P,
                Token::Open,
                Token::Open,
                Token::Special(c),
                Token::Close,
                Token::M,
                Token::Special(c),
                Token::Close,
                Token::M,
                Token::F,
                Token::Open,
                Token::M,
                Token::F,
                Token::Special(c),
                Token::Close,
                Token::P,
                Token::Special(c),
            ],
            Token::F => vec![Token::F, Token::F],
            _ => vec![self],
        }
    }
}

impl From<Token> for lsystem::Sym {
    fn from(value: Token) -> Self {
        match value {
            Token::F => lsystem::Sym::Forward,
            Token::P => lsystem::Sym::Plus,
            Token::M => lsystem::Sym::Minus,
            Token::Open => lsystem::Sym::Push,
            Token::Close => lsystem::Sym::Pop,
            Token::Special(_) => lsystem::Sym::Nop,
        }
    }
}

/// "Expression" syntax.
enum Expr {
    Instr(Sym),
    Parens(Vec<Expr>),
}
use Expr::*;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
enum Sym {
    F,
    P,
    M,
    Special(char),
}

impl Expr {
    fn max_height(exprs: &[Self]) -> usize {
        exprs.iter().map(|expr| expr.height()).max().unwrap_or(0)
    }

    fn height(&self) -> usize {
        match self {
            Instr(_) => 0,
            Parens(exprs) => 1 + Self::max_height(exprs),
        }
    }
}

impl Expr {
    /// Parse a stream of Syms to a list of Exprs.
    fn parse(input: &[Token]) -> Result<Vec<Expr>, Box<dyn std::error::Error>> {
        use combine::choice;
        use combine::eof;
        use combine::many;
        use combine::parser;
        use combine::satisfy_map;
        use combine::token;
        use combine::Parser;
        use combine::Stream;

        fn expr_<Input: Stream<Token = Token>>() -> impl Parser<Input, Output = Expr> {
            instr().or(parens())
        }

        parser! {
            fn expr[Input]()(Input) -> Expr
            where [Input: Stream<Token = Token>] {
                expr_()
            }
        }

        fn instr<Input: Stream<Token = Token>>() -> impl Parser<Input, Output = Expr> {
            choice!(
                token(Token::F).map(|_| Sym::F),
                token(Token::P).map(|_| Sym::P),
                token(Token::M).map(|_| Sym::M),
                satisfy_map(|s| match s {
                    Token::Special(c) => Some(Sym::Special(c)),
                    _ => None,
                })
            )
            .map(Instr)
        }

        fn parens<Input: Stream<Token = Token>>() -> impl Parser<Input, Output = Expr> {
            token(Token::Open)
                .with(many(expr()))
                .skip(token(Token::Close))
                .map(Parens)
        }

        many(expr())
            .skip(eof())
            .parse(input)
            .map(|u| u.0)
            .map_err(|e| e.into())
    }
}

fn branch_width(t: f64) -> f64 {
    8. * (1. - t)
}

fn run<F: FnMut(&mut System), G: FnOnce(&mut System)>(mut render: F, end: G) {
    let mut sys = System::new("Growing L-System", DIM, DIM);
    sys.draw(|gfx| {
        gfx.paint = paint::radial_gradient(
            Point::new((DIM - 1) as f64 / 2., (DIM - 1) as f64 / 2.),
            Color::white(),
            DIM as f64 - ((DIM - 1) as f64) / 2.,
            Color::from((0x00, 0xCC, 0xFF)),
        );
        gfx.paint_clear();
    });
    render(&mut sys);

    let init = Token::parse(STRING).unwrap();
    let instrs = Token::expands_n(init.into_iter(), ITERATIONS);
    // Compute the appropriate transform.
    let t = playgraphics::util::auto_place(
        DIM as f64,
        DIM as f64,
        PARAMS
            .run(instrs.iter().copied(), INIT)
            .flat_map(|(p, q)| [p, q]),
    );

    let exprs = Expr::parse(&instrs).unwrap();
    let height = Expr::max_height(&exprs);

    let stem_color = Color::from((0xC1, 0x9A, 0x6B));
    let leaf_color = Color::from((0x21, 0x42, 0x1E));

    let mut rng = thread_rng();
    let mut branches = BinaryHeap::<Keyed<_, (&[Expr], _, _)>>::new();
    branches.push(Keyed(
        NotNan::new(rng.gen_range(0. ..=1.)).unwrap(),
        (&exprs, INIT, 0),
    ));
    'OUTER: while let Some((exprs, mut state, current_height)) = branches.pop().map(|k| k.1) {
        for (i, expr) in exprs.iter().enumerate() {
            match expr {
                Instr(Sym::Special(_)) => (),
                Instr(Sym::P) => {
                    state.angle += PARAMS.angle;
                }
                Instr(Sym::M) => {
                    state.angle -= PARAMS.angle;
                }
                Parens(sub_exprs) => {
                    branches.push(Keyed(
                        NotNan::new(rng.gen_range(0. ..=1.)).unwrap(),
                        (&exprs[i + 1..], state, current_height + 1),
                    ));
                    branches.push(Keyed(
                        NotNan::new(rng.gen_range(0. ..=1.)).unwrap(),
                        (sub_exprs, state, current_height + 1),
                    ));
                    continue 'OUTER;
                }
                Instr(Sym::F) => {
                    let pos = state.pos;
                    state.pos += Linear::rotation(state.angle) * PARAMS.translation;
                    sys.draw(|gfx| {
                        gfx.transform = t;
                        let t = current_height as f64 / height as f64;
                        let dim = branch_width(t);
                        let corners = [
                            Affine::rotation(pos, state.angle + π / 2.)
                                * (pos + Vector::new(dim, 0.)),
                            Affine::rotation(state.pos, state.angle + π / 2.)
                                * (state.pos + Vector::new(dim, 0.)),
                            Affine::rotation(state.pos, state.angle - π / 2.)
                                * (state.pos + Vector::new(dim, 0.)),
                            Affine::rotation(pos, state.angle - π / 2.)
                                * (pos + Vector::new(dim, 0.)),
                        ];
                        let color = stem_color
                            .interpolate(leaf_color, current_height as f64 / height as f64);
                        gfx.paint = paint::intensity_noise(color, 0.05);
                        if dim == 0. {
                            gfx.paint_line(pos, state.pos)
                        } else {
                            gfx.paint_fill_polygon(&corners);
                        }
                    });
                    render(&mut sys);
                }
            }
        }
    }
    end(&mut sys);
}

#[derive(Debug, Parser)]
struct Args {
    #[arg(long, default_value = None)]
    out: Option<String>,
}

fn main() {
    let args = Args::parse();
    match args.out {
        None => run(
            |sys| {
                sys.render();
            },
            |sys| sys.park(),
        ),
        Some(dir) => {
            std::fs::create_dir_all(&dir).unwrap();
            let mut frame = 0;
            let f = |sys: &mut System| {
                let filename = {
                    let mut path = PathBuf::new();
                    path.push(dir.clone());
                    path.push(format!("{}", frame));
                    path.set_extension("png");
                    path
                };
                let screenshot = sys.screenshot();
                screenshot.save(filename).unwrap();
                frame += 1;
            };
            let g = |_: &mut System| ();
            run(f, g);
        }
    };
}
