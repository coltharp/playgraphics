use playgraphics::ifs;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use rand::prelude::*;
use rayon::prelude::ParallelIterator;
use rayon::prelude::*;

use std::f64::consts::PI;
const O: Point = Point::origin();
const NUM_POINTS: usize = 2_usize.pow(6);
const NUM_SKIP: usize = 2_usize.pow(5);
const NUM_TAKE: usize = 2_usize.pow(10);
const DIM: usize = 800;

fn main() {
    let rules: [Affine; 2] = [
        [
            Affine::dilation(O, 0.5),
            Affine::rotation(O, 3. * PI / 4.),
            Affine::translation_x(1. / 3.),
            Affine::translation_y(1. / 3.),
        ]
        .iter()
        .rev()
        .product(),
        [
            Affine::dilation(O, 0.5),
            Affine::rotation(O, -3. * PI / 4.),
            Affine::translation_x(1. / 3.),
            Affine::translation_y(1. / 3.),
        ]
        .iter()
        .rev()
        .product(),
    ];

    let inits = (0..NUM_POINTS)
        .into_par_iter()
        .map(|_| Point::new((random::<f64>() - 0.5).abs(), (random::<f64>() - 0.5).abs()));
    let points = ifs::run_slice_par(&rules, inits)
        .flat_map_iter(|it| it.skip(NUM_SKIP).take(NUM_TAKE))
        .map(|u| u.1)
        .collect::<Vec<_>>();

    let mut sys = System::new("IFS Triangle", 800, 800);
    sys.draw_and_render(|g| {
        g.transform = Affine::dilation(O, DIM as f64);
        g.color = Color::white();
        g.clear();
        g.color = Color::black();
        for p in points {
            g.draw_point(p);
        }
    });
    sys.park();
}
