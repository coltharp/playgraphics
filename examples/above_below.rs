#![allow(mixed_script_confusables)]

use clap::Parser;
use itertools::Itertools;
use playgraphics::point::ORIGIN as O;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;
use rand::prelude::*;
use rayon::prelude::*;
use std::f64::consts::PI as π;

struct Letter {
    width: f64,
    height: f64,
    // Given the total width and height of the enclosing space, return a vector
    // whose elements are the transforms making up this letter's strokes
    // (un-composed, in "reverse order"; i.e., the first element of each vector
    // is meant to be applied first).
    transforms: fn(f64, f64) -> Vec<Vec<Affine>>,
}

impl Letter {
    fn make_all_transforms(letters: &[Letter], spacing: f64) -> (f64, f64, Vec<Affine>) {
        let total_width =
            letters.iter().map(|l| l.width).sum::<f64>() + spacing * (letters.len() - 1) as f64;
        let total_height = letters
            .iter()
            .map(|l| l.height)
            .max_by(f64::total_cmp)
            .unwrap();
        let mut ret = Vec::new();
        let mut x = 0.;
        for letter in letters {
            for transform in letter.make_transforms(total_width, total_height) {
                ret.push(Affine::translation_x(x) * transform);
            }
            x += letter.width + spacing;
        }
        (total_width, total_height, ret)
    }

    fn make_transforms(&self, total_width: f64, total_height: f64) -> Vec<Affine> {
        (self.transforms)(total_width, total_height)
            .iter()
            .map(|ts| ts.iter().rev().product())
            .collect()
    }
}

// "A"
fn ua() -> Letter {
    Letter {
        width: 8.,
        height: 12.,
        transforms: |w, h| {
            vec![
                // left
                vec![
                    Affine::dilation_x(O, 12. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(O, 1. / 4.),
                ],
                // right
                vec![
                    Affine::dilation_x(O, 12. / w),
                    Affine::reflection_x(6.),
                    Affine::dilation_y(O, 1. / h),
                    Affine::reflection_y(0.5),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(O, 1. / 4.),
                    Affine::reflection_x(4.),
                ],
                // middle
                vec![
                    Affine::dilation_x(O, 2. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::translation(Vector::new(3., 7.)),
                ],
            ]
        },
    }
}

// "B"
fn ub() -> Letter {
    Letter {
        width: 4.,
        height: 12.,
        transforms: |w, h| {
            vec![
                // left
                vec![
                    Affine::dilation_x(O, 10. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::translation_y(1.),
                ],
                // bottom
                vec![
                    Affine::dilation_x(O, 4. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::reflection_x(2.),
                ],
                // top
                vec![
                    Affine::dilation_x(O, 4. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::reflection_y(6.),
                ],
                // bottom diagonal
                vec![
                    Affine::dilation_x(O, 5. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(O, -2. / 5.),
                    Affine::translation(Vector::new(3., 1.)),
                ],
                // top diagonal
                vec![
                    Affine::dilation_x(O, 5. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(O, -2. / 5.),
                    Affine::translation(Vector::new(3., 1.)),
                    Affine::reflection_y(6.),
                ],
            ]
        },
    }
}

// "O"
fn uo() -> Letter {
    Letter {
        width: 4.,
        height: 12.,
        transforms: |w, h| {
            vec![
                // left
                vec![
                    Affine::dilation_x(O, 11. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::reflection_x(5.5),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                ],
                // right
                vec![
                    Affine::dilation_x(O, 11. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::translation_y(1.),
                    Affine::reflection_x(2.),
                ],
                // bottom
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::reflection_y(0.5),
                ],
                // top
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::reflection_x(1.5),
                    Affine::translation_y(11.),
                ],
            ]
        },
    }
}

// "V"
fn uv() -> Letter {
    Letter {
        width: 8.,
        height: 12.,
        transforms: |w, h| {
            vec![
                // left
                vec![
                    Affine::dilation_x(O, 12. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(Point::new(0., 12.), -1. / 4.),
                ],
                // right
                vec![
                    Affine::dilation_x(O, 12. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(Point::new(0., 12.), -1. / 4.),
                    Affine::reflection_x(4.),
                ],
            ]
        },
    }
}

// "E"
fn ue() -> Letter {
    Letter {
        width: 4.,
        height: 12.,
        transforms: |w, h| {
            vec![
                // left
                vec![
                    Affine::dilation_x(O, 10. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::translation_y(1.),
                    Affine::reflection_x(0.5),
                    Affine::reflection_y(6.),
                ],
                // bottom
                vec![Affine::dilation_x(O, 4. / w), Affine::dilation_y(O, 1. / h)],
                // top
                vec![
                    Affine::dilation_x(O, 4. / w),
                    Affine::reflection_x(2.),
                    Affine::dilation_y(O, 1. / h),
                    Affine::reflection_y(6.),
                ],
                // middle
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::translation(Vector::new(1., 5.5)),
                ],
            ]
        },
    }
}

// "b"
fn lb() -> Letter {
    Letter {
        width: 4.,
        height: 12.,
        transforms: |w, h| {
            vec![
                // left top
                vec![
                    Affine::dilation_x(O, 8. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::translation_y(4.),
                ],
                // left bottom
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                ],
                // bottom
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::translation_x(1.),
                ],
                // right
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::translation(Vector::new(3., 1.)),
                ],
                // top
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::translation_y(3.),
                ],
            ]
        },
    }
}

// "e"
fn le() -> Letter {
    Letter {
        width: 4.,
        height: 5.,
        transforms: |w, h| {
            vec![
                // top
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::translation_y(4.),
                ],
                // middle
                vec![
                    Affine::dilation_x(O, 2. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::translation(Vector::new(1., 2.)),
                ],
                // right
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::translation(Vector::new(3., 2.)),
                ],
                // left
                vec![
                    Affine::dilation_x(O, 4. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                ],
                // bottom
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::translation_x(1.),
                ],
            ]
        },
    }
}

// "l"
fn ll() -> Letter {
    Letter {
        width: 4.,
        height: 4.,
        transforms: |w, h| {
            // left
            vec![
                vec![
                    Affine::dilation_x(O, 11. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::translation_y(1.),
                ],
                // bottom
                vec![Affine::dilation_x(O, 4. / w), Affine::dilation_y(O, 1. / h)],
            ]
        },
    }
}

// "o"
fn lo() -> Letter {
    Letter {
        width: 4.,
        height: 4.,
        transforms: |w, h| {
            vec![
                // left
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                ],
                // bottom
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::translation_x(1.),
                ],
                // right
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::translation(Vector::new(3., 1.)),
                ],
                // top
                vec![
                    Affine::dilation_x(O, 3. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::translation_y(3.),
                ],
            ]
        },
    }
}

// "w"
fn lw() -> Letter {
    Letter {
        width: 8.,
        height: 4.,
        transforms: |w, h| {
            vec![
                // first
                vec![
                    Affine::dilation_x(O, 4. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(Point::new(0., 4.), -0.25),
                ],
                // second
                vec![
                    Affine::dilation_x(O, 4. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(Point::new(0., 4.), -0.25),
                    Affine::reflection_x(2.),
                ],
                // third
                vec![
                    Affine::dilation_x(O, 4. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(Point::new(0., 4.), -0.25),
                    Affine::translation_x(3.),
                ],
                // fourth
                vec![
                    Affine::dilation_x(O, 4. / w),
                    Affine::dilation_y(O, 1. / h),
                    Affine::rotation(Point::new(0.5, 0.5), π / 2.),
                    Affine::shear_x(Point::new(0., 4.), -0.25),
                    Affine::reflection_x(2.),
                    Affine::translation_x(3.),
                ],
            ]
        },
    }
}

const NUM_POINTS: usize = 128;
const NUM_SKIP: usize = 32;
const NUM_ITERATIONS: usize = 8192;
const DIM: usize = 2000;
const HORIZONTAL_SPACING: f64 = 1.;
const VERTICAL_SPACING: f64 = 12.;

fn run<F: Fn(&mut System), G: FnOnce(&mut System)>(render: F, end: G) {
    let mut sys = System::new("ABOVE/below", DIM, DIM);
    let above_letters = [ua(), ub(), uo(), uv(), ue()];
    let (above_width, _, above_rules) =
        Letter::make_all_transforms(&above_letters, HORIZONTAL_SPACING);
    let below_letters = [lb(), le(), ll(), lo(), lw()];
    let (below_width, below_height, below_rules) =
        Letter::make_all_transforms(&below_letters, HORIZONTAL_SPACING);

    let below_to_above = Affine::translation(Vector::new(
        -above_width.max(below_width) / 2.,
        below_height + VERTICAL_SPACING,
    ));

    let above_rules = above_rules.iter().map(|t| t * below_to_above).collect_vec();
    let below_rules = below_rules
        .iter()
        .map(|t| below_to_above.inverse() * t)
        .collect_vec();

    let points = (0..NUM_POINTS)
        .into_par_iter()
        .flat_map_iter(|_| {
            let mut rng = thread_rng();
            let p = if rng.gen() {
                Point::new(rng.gen_range(0. ..=1.), rng.gen_range(0. ..=1.))
            } else {
                Point::new(rng.gen_range(0. ..=1.), rng.gen_range(-10. ..=0.))
            };
            itertools::unfold((p, false), |(p, s)| {
                let rules = if *s { &above_rules } else { &below_rules };
                let f = rules.choose(&mut rng).unwrap();
                *p = f * *p;
                *s = !*s;
                Some(*p)
            })
            .skip(NUM_SKIP)
            .take(NUM_ITERATIONS)
            .collect_vec()
        })
        .collect::<Vec<_>>();
    let t = playgraphics::util::auto_place(DIM as f64, DIM as f64, points.iter().copied());

    let ruby = Color::from_0rgb(0xE0115F);
    let emerald = Color::from_0rgb(0x50C878);
    let gold = Color::from_0rgb(0xFFD700);
    sys.draw(|gfx| {
        gfx.color = Color::white();
        gfx.clear();
        gfx.transform = t;
        gfx.color = Color::black();
        for p in points {
            gfx.draw_point(p);
        }
        gfx.paint = Box::new(move |p, c| {
            let t = (DIM as f64 - p.y) / DIM as f64;
            let color = if t < 0.5 {
                ruby.interpolate(gold, 2. * t)
            } else if t == 0.5 {
                gold
            } else {
                gold.interpolate(emerald, 2. * (t - 0.5))
            };
            if c == Color::white() {
                color
            } else {
                color.opposite()
            }
        });
        gfx.paint_clear();
    });
    render(&mut sys);
    end(&mut sys);
}

#[derive(Debug, Parser)]
struct Args {
    #[arg(long, default_value = None)]
    out: Option<String>,
}

fn main() {
    let args = Args::parse();
    match args.out {
        None => run(
            |sys| {
                sys.render();
            },
            |sys| sys.park(),
        ),
        Some(out) => run(
            |sys: &mut System| {
                let screenshot = sys.screenshot();
                screenshot.save(&out).unwrap();
            },
            |_| (),
        ),
    }
}
