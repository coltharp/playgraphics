use playgraphics::Color;
use playgraphics::Grammar;
use playgraphics::Linear;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;

struct G;

#[derive(Clone, Copy)]
enum S {
    A,
    F,
    Plus,
    Minus,
    Open,
    Close,
}
use S::*;

impl Grammar for G {
    type Sym = S;

    fn translate(s: Self::Sym) -> Vec<Self::Sym> {
        match s {
            A => vec![
                F, Minus, Open, Open, A, Close, Plus, A, Close, Plus, F, Open, Plus, F, A, Close,
                Minus, A,
            ],
            F => vec![F, F],
            _ => vec![s],
        }
    }
}

const AXIOM: [S; 1] = [A];

const MOVE: Vector = Vector::new(1.0, 0.0);
const ANGLE: f64 = 2.0 * std::f64::consts::PI * (22.5 / 360.);

const WIDTH: usize = 800;
const HEIGHT: usize = 800;

const NUM_ITERATIONS: usize = 9;

#[derive(Clone, Copy)]
struct State {
    pos: Point,
    angle: f64,
}

impl State {
    fn new() -> Self {
        State {
            pos: Point::default(),
            angle: 0.0,
        }
    }
}

fn main() {
    let str = G::expand(&AXIOM, NUM_ITERATIONS);

    let mut stack = vec![State::new()];
    let mut points = Vec::with_capacity(str.len() + 1);
    points.push(Point::origin());
    for s in str {
        let mut st = stack.pop().unwrap();
        match s {
            F => st.pos += Linear::rotation(st.angle) * MOVE,
            Plus => st.angle += ANGLE,
            Minus => st.angle -= ANGLE,
            Open => stack.push(st),
            Close => continue,
            _ => (),
        }
        points.push(st.pos);
        stack.push(st);
    }
    let t = playgraphics::util::auto_place(WIDTH as f64, HEIGHT as f64, points.iter().copied());
    for p in &mut points {
        *p *= t;
    }

    let mut sys = System::new("Leaf", 800, 800);
    sys.draw_and_render(|g| {
        g.color = Color::black();
        g.clear();
        g.color = Color::green();
        for p in points {
            g.draw_point(p);
        }
    });
    sys.park();
}
