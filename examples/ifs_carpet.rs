use itertools::Itertools;
use playgraphics::ifs;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;

const ITERATIONS: usize = 1_000_000;
const DIM: usize = 800;

fn main() {
    let rules = [
        Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(1. / 3., 0.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(1. / 3., 2. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(2. / 3., 0.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(2. / 3., 1. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(0., 1. / 3.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(0., 2. / 3.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(2. / 3., 2. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
    ];

    let p = Point::new(rand::random(), rand::random());
    let points = ifs::run_slice(&rules, p)
        .take(ITERATIONS)
        .map(|u| u.1)
        .collect_vec();

    let mut sys = System::new("IFS Carpet", DIM, DIM);
    let t = Affine::dilation(Point::origin(), DIM as f64);

    sys.draw_and_render(|g| {
        g.color = Color::white();
        g.clear();
        g.color = Color::black();
        for p in points {
            g.draw_point(t * p);
        }
    });
    sys.park();
}
