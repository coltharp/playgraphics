use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Graphics;
use playgraphics::MouseButton;
use playgraphics::Point;
use playgraphics::System;

fn draw_koch_curve(g: &mut Graphics, a: Point, e: Point, limit: usize) {
    if limit == 0 {
        g.draw_line(a, e);
        return;
    }
    let vec = (e - a) / 3.;
    let b = a + vec;
    let d = a + 2. * vec;
    let c = Affine::rotation(b, std::f64::consts::PI / 3.) * d;
    draw_koch_curve(g, a, b, limit - 1);
    draw_koch_curve(g, b, c, limit - 1);
    draw_koch_curve(g, b, c, limit - 1);
    draw_koch_curve(g, c, d, limit - 1);
    draw_koch_curve(g, d, e, limit - 1);
}

fn main() {
    let mut sys = System::new("Koch curve", 800, 800);
    sys.draw_and_render(|g| {
        g.color = Color::white();
        g.clear()
    });
    let p = sys.wait_click(MouseButton::Left);
    loop {
        let q = sys.mouse_pos();
        sys.draw_and_render(|g| {
            g.color = Color::white();
            g.clear();
            g.color = Color::black();
            draw_koch_curve(g, p, q, 8);
        });
    }
}
