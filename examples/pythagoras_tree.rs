use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Event;
use playgraphics::Graphics;
use playgraphics::MouseButton;
use playgraphics::Point;
use playgraphics::System;

fn draw_pythagoras_tree(g: &mut Graphics, a: Point, b: Point, angle: f64, limit: usize) {
    let c = Affine::rotation(b, -std::f64::consts::PI / 2.) * a;
    let d = Affine::rotation(a, std::f64::consts::PI / 2.) * b;
    g.fill_polygon(&[a, b, c, d]);
    if limit == 0 {
        return;
    }
    let cd = c.midpoint(d);
    let e = Affine::rotation(cd, angle) * c;
    draw_pythagoras_tree(g, d, e, std::f64::consts::PI - angle, limit - 1);
    draw_pythagoras_tree(g, e, c, angle, limit - 1);
}

fn main() {
    let mut sys = System::new("Pythagoras tree", 800, 800);
    sys.draw_and_render(|g| {
        g.color = Color::white();
        g.clear()
    });
    let a = sys.wait_click(MouseButton::Left);
    let b = loop {
        let b = sys.mouse_pos();
        let events = sys.draw_and_render(|g| {
            g.color = Color::white();
            g.clear();
            g.color = Color::white();
            let c = Affine::rotation(b, -std::f64::consts::PI / 2.) * a;
            let d = Affine::rotation(a, std::f64::consts::PI / 2.) * b;
            g.fill_polygon(&[a, b, c, d]);
        });
        if events.contains(&Event::MouseDown(MouseButton::Left)) {
            break b;
        }
    };
    let ab = a.midpoint(b);
    loop {
        let e = sys.mouse_pos();
        let angle = a.angle_from(ab, e);
        sys.draw_and_render(|g| {
            g.color = Color::white();
            g.clear();
            g.color = Color::black();
            draw_pythagoras_tree(g, a, b, angle, 12);
        });
    }
}
