use playgraphics::ifs;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;
use rand::prelude::*;
use rayon::prelude::*;

use std::f64::consts::PI;
const O: Point = Point::origin();

const LETTER_HEIGHT: f64 = 4.;

struct Letter {
    width: f64,
    transforms: fn(f64, f64) -> Vec<Affine>,
}

const SPACING: f64 = 1.;

impl Letter {
    fn make_transforms(letters: &[Letter]) -> (f64, f64, Vec<Affine>) {
        let total_width =
            letters.iter().map(|l| l.width).sum::<f64>() + SPACING * (letters.len() - 1) as f64;
        let total_height = LETTER_HEIGHT;
        let mut ret = Vec::new();
        let mut x = 0.;
        for letter in letters {
            for transform in (letter.transforms)(total_width, total_height) {
                ret.push(Affine::translation_x(x) * transform);
            }
            x += letter.width + SPACING;
        }
        (total_width, total_height, ret)
    }
}

fn n() -> Letter {
    Letter {
        width: 4.,
        transforms: |total_width, total_height| {
            vec![
                // left
                [
                    Affine::dilation_x(O, 4. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(Point::new(0.5, 0.5), PI / 2.),
                ]
                .iter()
                .rev()
                .product(),
                // middle top
                [
                    Affine::dilation_x(O, 1. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::translation(Vector::new(1., 2.)),
                ]
                .iter()
                .rev()
                .product(),
                [
                    Affine::dilation_x(O, 1. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::translation(Vector::new(2., 1.)),
                ]
                .iter()
                .rev()
                .product(),
                // right
                [
                    Affine::dilation_x(O, 4. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(Point::new(0.5, 0.5), PI / 2.),
                    Affine::translation_x(3.),
                ]
                .iter()
                .rev()
                .product(),
            ]
        },
    }
}

fn g() -> Letter {
    Letter {
        width: 4.,
        transforms: |total_width, total_height| {
            vec![
                // top
                [
                    Affine::dilation_x(O, 4. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(Point::new(2., 0.5), PI),
                    Affine::translation_y(3.),
                ]
                .iter()
                .rev()
                .product(),
                // left
                [
                    Affine::dilation_x(O, 3. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(Point::new(0., 0.), -PI / 2.),
                    Affine::translation_y(3.),
                ]
                .iter()
                .rev()
                .product(),
                // bottom
                [
                    Affine::dilation_x(O, 3. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::translation_x(1.0),
                ]
                .iter()
                .rev()
                .product(),
                // right
                [
                    Affine::dilation_x(O, 1. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(Point::new(0.5, 0.5), PI / 2.),
                    Affine::translation(Vector::new(3., 1.)),
                ]
                .iter()
                .rev()
                .product(),
            ]
        },
    }
}

fn p() -> Letter {
    Letter {
        width: 4.,
        transforms: |total_width, total_height| {
            vec![
                // left
                [
                    Affine::dilation_x(O, 4. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(Point::new(0.5, 0.5), PI / 2.),
                ]
                .iter()
                .rev()
                .product(),
                // top
                [
                    Affine::dilation_x(O, 3. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::translation(Vector::new(1., 3.)),
                ]
                .iter()
                .rev()
                .product(),
                // right
                [
                    Affine::dilation_x(O, 2. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(O, -PI / 2.),
                    Affine::translation(Vector::new(3., 3.)),
                ]
                .iter()
                .rev()
                .product(),
                // middle
                [
                    Affine::dilation_x(O, 2. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(O, PI),
                    Affine::translation(Vector::new(3., 2.)),
                ]
                .iter()
                .rev()
                .product(),
            ]
        },
    }
}

fn c() -> Letter {
    Letter {
        width: 4.,
        transforms: |total_width, total_height| {
            vec![
                // top
                [
                    Affine::dilation_x(O, 4. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(Point::new(2., 0.5), PI),
                    Affine::translation_y(3.),
                ]
                .iter()
                .rev()
                .product(),
                // left
                [
                    Affine::dilation_x(O, 3. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::rotation(Point::new(0., 0.), -PI / 2.),
                    Affine::translation_y(3.),
                ]
                .iter()
                .rev()
                .product(),
                // bottom
                [
                    Affine::dilation_x(O, 3. / total_width),
                    Affine::dilation_y(O, 1. / total_height),
                    Affine::translation_x(1.0),
                ]
                .iter()
                .rev()
                .product(),
            ]
        },
    }
}

fn main() {
    let num_points = 100;
    let num_iterations = 10_000;
    let dim = 800;

    let letters = [n(), g(), p(), c()];
    let inits = (0..num_points)
        .into_par_iter()
        .map(|_| Point::new(random::<f64>(), random::<f64>()));
    let (_, _, rules) = Letter::make_transforms(&letters);
    let points = ifs::run_slice_par(&rules, inits)
        .flat_map_iter(|it| it.skip(30).take(num_iterations))
        .map(|u| u.1)
        .collect::<Vec<_>>();

    let mut sys = System::new("IFS Initials", dim, dim);
    sys.draw_and_render(|g| {
        g.transform =
            playgraphics::util::auto_place(dim as f64, dim as f64, points.iter().copied());
        g.color = Color::white();
        g.clear();
        g.color = Color::black();
        for p in points {
            g.draw_point(p);
        }
    });

    sys.park();
}
