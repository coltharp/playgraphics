#![allow(dead_code)]
#![allow(mixed_script_confusables)]

use clap::Parser;
use ndarray::prelude::*;
use ndarray::Zip;
use num::Complex;
use num::Zero;
use playgraphics::point::ORIGIN as O;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Event::*;
use playgraphics::Graphics;
use playgraphics::MouseButton;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::UpDownState::*;
use playgraphics::Vector;
use std::f64::consts::PI as π;
use std::path::PathBuf;

fn mandelbrot(points: &Array2<Complex<f64>>, iterations: usize, out: &mut Array2<Option<usize>>) {
    Zip::from(points).par_map_assign_into(out, |c| {
        let mut z = Complex::<f64>::zero();
        for i in 0..iterations {
            if z.norm_sqr() >= 4. {
                return Some(i);
            }
            z = z.powi(2) + c;
        }
        None
    });
}

fn julia(
    c: Complex<f64>,
    points: &Array2<Complex<f64>>,
    iterations: usize,
    out: &mut Array2<Option<usize>>,
) {
    Zip::from(points).par_map_assign_into(out, |z| {
        let mut z = *z;
        for i in 0..iterations {
            if z.norm_sqr() >= 4. {
                return Some(i);
            }
            z = z.powi(2) + c;
        }
        None
    });
}

fn logistic(points: &[f64], out: &mut Array2<f64>) {
    Zip::from(points)
        .and(out.rows_mut())
        .par_for_each(|r, row| {
            let mut x = 0.;
            for out in row {
                x = r * x * (1. - x);
                *out = x;
            }
        });
}

const COLOR_LOOP: usize = 64;

fn draw_points(gfx: &mut Graphics, points: &Array2<Option<usize>>) {
    points.indexed_iter().for_each(|((i, j), v)| {
        gfx.color = match v {
            None => Color::black(),
            &Some(n) => {
                let t = n % COLOR_LOOP;
                if t < COLOR_LOOP / 2 {
                    let r = t as f64 / (COLOR_LOOP / 2) as f64;
                    let g = r;
                    let b = 0.5 + t as f64 / COLOR_LOOP as f64;
                    Color::new(r, g, b).unwrap()
                } else {
                    let t = COLOR_LOOP - t;
                    let r = 0.5 + t as f64 / COLOR_LOOP as f64;
                    let g = t as f64 / (COLOR_LOOP / 2) as f64;
                    let b = g;
                    Color::new(r, g, b).unwrap()
                }
            }
        };
        gfx.draw_point(Point::new(i as f64, j as f64));
    });
}

const PANE_DIM: usize = 800;
const BORDER_WIDTH: usize = 0;
const BASE_ITERATIONS: usize = 256;

fn run_interactive() -> ! {
    let mut sys = System::new(
        "Mandelbrot + Julia",
        PANE_DIM + BORDER_WIDTH + PANE_DIM,
        PANE_DIM,
    );
    let mut mouse_pos = sys.mouse_pos();
    let mut out = Array2::default((PANE_DIM, PANE_DIM));

    let mut window_to_mandelbrot =
        Affine::translation(Vector::new(-2., -2.)) * Affine::dilation(O, 4. / PANE_DIM as f64);
    let mut mandelbrot_points = Array2::from_shape_fn((PANE_DIM, PANE_DIM), |(i, j)| Complex {
        re: (4. * i as f64 / PANE_DIM as f64) - 2.,
        im: (4. * j as f64 / PANE_DIM as f64) - 2.,
    });
    mandelbrot(&mandelbrot_points, BASE_ITERATIONS, &mut out);
    sys.draw(|gfx| draw_points(gfx, &out));

    let julia_points = mandelbrot_points.clone();
    let to_right_pane = Affine::translation_x((PANE_DIM + BORDER_WIDTH) as f64);
    let c = Complex::from(window_to_mandelbrot * mouse_pos);
    julia(c, &julia_points, BASE_ITERATIONS, &mut out);
    sys.draw(|gfx| {
        gfx.transform = to_right_pane;
        draw_points(gfx, &out);
    });

    let mut zoom_this_frame: Option<f64> = None;
    let mut total_zoom = 1.0;
    let mut mouse_button_state = Up;
    let mut events = sys.render();
    loop {
        let mut need_mandelbrot = false;
        let mut need_julia = false;

        for event in events {
            match event {
                MouseDown(MouseButton::Left) => {
                    mouse_button_state = Down;
                }
                MouseUp(MouseButton::Left) => {
                    mouse_button_state = Up;
                }
                KeyDown(_, '+') => {
                    zoom_this_frame = Some(2.);
                }
                KeyDown(_, '-') => {
                    zoom_this_frame = Some(0.5);
                }
                Scroll(v) => {
                    // At least on Mac, if there is ANY x-component to the
                    // scroll, it tends to have an unpredictable y-value.
                    if v.x == 0. {
                        zoom_this_frame = Some(1.1);
                    }
                }
                MouseMove(p) => {
                    if mouse_button_state == Down {
                        let t = Affine::translation(window_to_mandelbrot.linear * (mouse_pos - p));
                        mandelbrot_points.par_mapv_inplace(|c| Complex::from(t * Point::from(c)));
                        window_to_mandelbrot = t * window_to_mandelbrot;
                        need_mandelbrot = true;
                    }
                    mouse_pos = p;
                    need_julia = true;
                }
                _ => (),
            }
        }

        let iterations = (total_zoom * BASE_ITERATIONS as f64) as usize;

        if let Some(d) = zoom_this_frame {
            let t = Affine::dilation(window_to_mandelbrot * mouse_pos, d.recip());
            mandelbrot_points.par_mapv_inplace(|c| Complex::from(t * Point::from(c)));
            window_to_mandelbrot = t * window_to_mandelbrot;
            need_mandelbrot = true;
            zoom_this_frame = None;
            total_zoom *= d;
        }

        if need_mandelbrot {
            mandelbrot(&mandelbrot_points, iterations, &mut out);
            sys.draw(|gfx| draw_points(gfx, &out));
        }
        if need_julia {
            let c = Complex::<f64>::from(window_to_mandelbrot * mouse_pos);
            julia(c, &julia_points, iterations, &mut out);
            sys.draw(|gfx| {
                gfx.transform = to_right_pane;
                draw_points(gfx, &out);
            });
        }

        if need_mandelbrot || need_julia {
            events = sys.render();
        } else {
            events = sys.update();
        }
    }
}

const NUM_FRAMES: usize = 512;

fn run_noninteractive(dir: &str) {
    std::fs::create_dir_all(dir).unwrap();
    let digits = (NUM_FRAMES as f64).log10().floor() as usize;

    let mut buf = [0_u32; 2 * PANE_DIM * PANE_DIM];
    let mut gfx = Graphics::new(&mut buf, PANE_DIM, 2 * PANE_DIM);
    let mut out = Array2::default((PANE_DIM, PANE_DIM));

    let points = Array2::from_shape_fn((PANE_DIM, PANE_DIM), |(i, j)| Complex {
        re: (4. * i as f64 / PANE_DIM as f64) - 2.,
        im: (4. * j as f64 / PANE_DIM as f64) - 2.,
    });
    mandelbrot(&points, BASE_ITERATIONS, &mut out);
    gfx.transform = Affine::translation_y(PANE_DIM as f64);
    draw_points(&mut gfx, &out);
    gfx.transform = Affine::identity();

    let mut frame = 0;
    loop {
        let t = 2. * π * (frame as f64 / NUM_FRAMES as f64);
        let c = 1.1 * (Complex::exp(Complex::<f64>::i() * Complex::from(t)) / 2.
            - Complex::exp(2. * Complex::i() * Complex::from(t)) / 4.);
        julia(c, &points, BASE_ITERATIONS, &mut out);
        draw_points(&mut gfx, &out);

        let filename = {
            let mut path = PathBuf::new();
            path.push(dir);
            path.push(format!("{:0digits$}", frame));
            path.set_extension("png");
            path
        };
        let screenshot = gfx.screenshot();
        screenshot.save(filename).unwrap();

        frame += 1;
        if frame == NUM_FRAMES {
            break;
        }
    }
}

#[derive(Debug, Parser)]
struct Args {
    #[arg(long, default_value = None)]
    out: Option<String>,
}

fn main() {
    let args = Args::parse();
    match args.out {
        None => run_interactive(),
        Some(dir) => run_noninteractive(&dir),
    }
}
