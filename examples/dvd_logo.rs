use clap::Parser;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Time;
use playgraphics::Vector;
use std::f64::consts::PI;
use std::path::PathBuf;

const LIMIT: usize = 8;
const WIDTH: f64 = 800.;
const HEIGHT: f64 = 800.;
const CENTER: Point = Point::new((WIDTH - 1.) / 2., (HEIGHT - 1.) / 2.);
const INIT_VELOCITY: Vector = Vector::new(250., 250.);
const ANGULAR_VELOCITY: f64 = 1.;
const COLOR_VELOCITY: f64 = -25.;
const COLOR_WAVELENGTH: f64 = 50.;
const RED_OFFSET: f64 = 0.;
const GREEN_OFFSET: f64 = 8. / 3.;
const BLUE_OFFSET: f64 = 16. / 3.;

fn build_sierpinski_triangle(
    p: Point,
    q: Point,
    r: Point,
    limit: usize,
    out: &mut Vec<[Point; 3]>,
) {
    if limit == 0 {
        return;
    }
    out.push([p, q, r]);
    let pq = p.midpoint(q);
    let qr = q.midpoint(r);
    let rp = r.midpoint(p);
    build_sierpinski_triangle(p, pq, rp, limit - 1, out);
    build_sierpinski_triangle(q, pq, qr, limit - 1, out);
    build_sierpinski_triangle(r, qr, rp, limit - 1, out);
}

fn run<T: Time, F: FnMut(&mut System)>(mut time: T, frames: Option<usize>, mut f: F) {
    let p = Point::new(500., 100.);
    let q = Point::new(300., 300.);
    let r = Affine::rotation(q, PI / 3.) * p;

    let mut triangles = Vec::new();
    // If we draw with recursion depth n, the number of triangles is
    //
    // 1 + 3 + 9 + 27 + ... + 3^(n - 1) = (3^n - 1)/2
    triangles.reserve((3_usize.pow(LIMIT as u32) - 1) / 2);
    build_sierpinski_triangle(p, q, r, LIMIT, &mut triangles);
    let mut velocity = INIT_VELOCITY;
    let mut sys = System::new("Sierpinski DVD Logo", WIDTH as usize, HEIGHT as usize);
    let mut color_phase = 0.;
    let mut frame = 0;
    loop {
        let dt = time.update().as_millis() as f64 / 1000.;
        // Compute an affine transform to move the figure.
        let t = {
            // Center of the figure.
            let c = Point::origin()
                + triangles[0]
                    .iter()
                    .map(|p| p - Point::origin())
                    .sum::<Vector>()
                    / 3.;
            let mut t =
                Affine::translation(dt * velocity) * Affine::rotation(c, dt * ANGULAR_VELOCITY);
            // Speculatively move the corners.
            let new_corners = triangles[0].map(|p| t * p);
            // Check for collisions.  If we find any, reposition and change velocity
            // accordingly.
            for u in new_corners {
                if u.x < 0. {
                    t *= Affine::translation_x(-u.x);
                    velocity.x = -velocity.x;
                } else if u.x >= WIDTH {
                    t *= Affine::translation_x(WIDTH - u.x);
                    velocity.x = -velocity.x;
                }
                if u.y < 0. {
                    t *= Affine::translation_y(-u.y);
                    velocity.y = -velocity.y;
                } else if u.y > HEIGHT {
                    t *= Affine::translation_y(HEIGHT - u.y);
                    velocity.y = -velocity.y;
                }
            }
            // Now we have the final transform.
            t
        };
        // Move the figure according to the transform we just computed.
        for [p, q, r] in &mut triangles {
            *p *= t;
            *q *= t;
            *r *= t;
        }
        color_phase += dt * COLOR_VELOCITY * 2. * PI;
        let paint = Box::new(move |p: Point, _| {
            let phase = color_phase + (p - CENTER).norm();
            Color::new(
                (((phase + RED_OFFSET * 2. * PI) / COLOR_WAVELENGTH).sin() + 1.) / 2.,
                (((phase + GREEN_OFFSET * 2. * PI) / COLOR_WAVELENGTH).sin() + 1.) / 2.,
                (((phase + BLUE_OFFSET * 2. * PI) / COLOR_WAVELENGTH).sin() + 1.) / 2.,
            )
            .unwrap()
        });
        sys.draw(|g| {
            g.color = Color::black();
            g.clear();
            g.paint = paint.clone();
            for tr in &triangles {
                g.paint_polygon(tr);
            }
        });
        f(&mut sys);
        frame += 1;
        if let Some(frames) = frames {
            if frame == frames {
                break;
            }
        }
    }
}

#[derive(Debug, Parser)]
struct Args {
    #[arg(long, default_value = "out")]
    out: String,
    #[arg(long, default_value = None)]
    frames: Option<usize>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::parse();
    match args.frames {
        None => run(std::time::Instant::now(), None, |sys| {
            sys.render();
        }),
        Some(frames) => {
            let digits = (frames as f64).log10().floor() as usize;
            std::fs::create_dir_all(&args.out)?;
            let mut frame = 0;
            run(
                playgraphics::time::VirtualTime {
                    duration: std::time::Duration::from_millis(3),
                },
                Some(frames),
                |sys| {
                    let screenshot = sys.screenshot();
                    let filename = {
                        let mut path = PathBuf::new();
                        path.push(&args.out);
                        path.push(format!("{:0digits$}", frame));
                        path.set_extension("png");
                        path
                    };
                    screenshot.save(filename).unwrap();
                    frame += 1;
                },
            );
        }
    };
    Ok(())
}
