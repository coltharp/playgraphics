#![allow(dead_code)]
#![allow(mixed_script_confusables)]
#![allow(non_snake_case)]

mod variations;

// Source: "The Fractal Flame Algorithm", Scott Draves, Erik Reckase
// https://flam3.com/flame_draves.pdf

use core_extensions::callable::*;
use core_extensions::impl_call;
use itertools::Itertools;
use lazy_static::lazy_static;
use ndarray::prelude::*;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Linear;
use playgraphics::MouseButton;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;
use rand::prelude::*;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};
use sha2::Digest;
use sha2::Sha256;
use std::fmt::Write;
use std::fs::File;
use std::path::PathBuf;
use std::sync::atomic::Ordering;

type Counter = u64;
type AtomicCounter = std::sync::atomic::AtomicU64;
// Four components: red, green, blue, and "monochrome".
type FlameBuf = Array2<(AtomicCounter, AtomicCounter, AtomicCounter, AtomicCounter)>;

const O: Point = Point::origin();
const COLOR_RESOLUTION: usize = 256;

fn random_color<R>(rng: &mut R) -> Color
where
    R: Rng,
{
    Color::new(
        rng.gen_range(0_f64..=1_f64),
        rng.gen_range(0_f64..=1_f64),
        rng.gen_range(0_f64..=1_f64),
    )
    .unwrap()
}

#[derive(Serialize, Deserialize)]
struct Function {
    pre: Affine,
    // Coefficients, one for each variation.
    coeffs: [f64; variations::NUM],
    post: Affine,
    color: Color,
}

impl_call! {
    fn ref_call(self: Function, p: Point) -> Point {
        let p = self.pre * p;
        self.post *
            (O + self.coeffs.iter().zip(variations::ALL.iter()).map(
                |(c, f)| c * (f(p, self.pre) - O)).sum::<Vector>())
    }
}

#[derive(Serialize, Deserialize)]
struct Flame {
    functions: Vec<Function>,
    global_transform: Affine,
}

impl Flame {
    fn run(&self, points_per_dim: usize, num_skip: usize, num_take: usize, buf: &mut FlameBuf) {
        (0..points_per_dim).into_par_iter().for_each(|i| {
            let x = (1. - i as f64 / points_per_dim as f64) * -1.
                + (i as f64 / points_per_dim as f64) * 1.;
            (0..points_per_dim).for_each(|j| {
                let y = (1. - j as f64 / points_per_dim as f64) * -1.
                    + (j as f64 / points_per_dim as f64) * 1.;
                let mut p = Point::new(x, y);
                let mut rng = thread_rng();
                let mut color = random_color(&mut rng);
                for _ in 0..num_skip {
                    let f = self.functions.choose(&mut rng).unwrap();
                    p = f.ref_call(p);
                    color = color.average(f.color);
                }
                for _ in 0..num_take {
                    let f = self.functions.choose(&mut rng).unwrap();
                    p = f.ref_call_(p);
                    color = color.average(f.color);
                    let pg = self.global_transform * p;
                    let x = pg.x.round();
                    let y = pg.y.round();
                    if x < 0. || y < 0. || x >= buf.dim().0 as f64 || y >= buf.dim().1 as f64 {
                        continue;
                    }
                    let x = x as usize;
                    let y = y as usize;
                    let (r, g, b, c) = &buf[(x, y)];
                    r.fetch_add(
                        (color.r() * COLOR_RESOLUTION as f64) as Counter,
                        Ordering::Relaxed,
                    );
                    g.fetch_add(
                        (color.g() * COLOR_RESOLUTION as f64) as Counter,
                        Ordering::Relaxed,
                    );
                    b.fetch_add(
                        (color.b() * COLOR_RESOLUTION as f64) as Counter,
                        Ordering::Relaxed,
                    );
                    c.fetch_add(1, Ordering::Relaxed);
                }
            })
        })
    }

    fn hash(&self) -> Vec<u8> {
        fn hash_affine(t: Affine, hasher: &mut Sha256) {
            for row in t.linear.elems {
                for elem in row {
                    Digest::update(hasher, elem.to_le_bytes());
                }
            }
            Digest::update(hasher, t.translation.x.to_le_bytes());
            Digest::update(hasher, t.translation.y.to_le_bytes());
        }

        let mut hasher = Sha256::new();
        Digest::update(&mut hasher, self.functions.len().to_le_bytes());
        for f in &self.functions {
            hash_affine(f.pre, &mut hasher);
            for coeff in f.coeffs {
                Digest::update(&mut hasher, coeff.to_le_bytes());
            }
            hash_affine(f.post, &mut hasher);
            Digest::update(&mut hasher, f.color.r().to_le_bytes());
            Digest::update(&mut hasher, f.color.g().to_le_bytes());
            Digest::update(&mut hasher, f.color.b().to_le_bytes());
        }
        hash_affine(self.global_transform, &mut hasher);

        Vec::from_iter(hasher.finalize().into_iter())
    }
}

fn random_variation_indices() -> [bool; variations::NUM] {
    let mut ret = [false; variations::NUM];
    for b in &mut ret {
        *b = random();
    }
    ret
}

fn random_function_coeffs(indices: &[bool]) -> [f64; variations::NUM] {
    let mut coeffs = [0.; variations::NUM];
    for (i, c) in coeffs.iter_mut().enumerate() {
        if indices[i] {
            *c = random();
        }
    }
    let sum = coeffs.iter().sum::<f64>();
    for c in &mut coeffs {
        *c /= sum;
    }
    coeffs
}

fn random_flame_from_rules<I>(rules: I) -> Flame
where
    I: Iterator<Item = (Affine, Affine)>,
{
    let indices = random_variation_indices();
    let functions = rules
        .map(|(post, pre)| {
            let coeffs = random_function_coeffs(&indices);
            Function {
                pre,
                coeffs,
                post,
                color: random_color(&mut thread_rng()),
            }
        })
        .collect_vec();
    assert!(!functions.is_empty());
    Flame {
        functions,
        global_transform: Affine::identity(),
    }
}

fn random_fractal_flame_from_rules(num_functions: usize, rules: &[Affine]) -> Flame {
    assert!(num_functions >= 1);
    let indices = random_variation_indices();
    let coeffss = (0..num_functions)
        .map(|_| random_function_coeffs(&indices))
        .collect_vec();
    let functions = rules
        .iter()
        .flat_map(|rule| {
            coeffss.iter().map(|coeffs| Function {
                pre: Affine::identity(),
                coeffs: *coeffs,
                post: *rule,
                color: random_color(&mut thread_rng()),
            })
        })
        .collect_vec();
    Flame {
        functions,
        global_transform: Affine::identity(),
    }
}

fn random_affine() -> Affine {
    let mut rng = thread_rng();
    let linear = if rng.gen() {
        Linear::identity()
    } else {
        Linear {
            elems: [
                [rng.gen_range(-2_f64..=2_f64), rng.gen_range(-2_f64..=2_f64)],
                [rng.gen_range(-2_f64..=2_f64), rng.gen_range(-2_f64..=2_f64)],
            ],
        }
    };
    let translation = if rng.gen() {
        Vector::default()
    } else {
        Vector {
            x: rng.gen_range(-0.1_f64..=0.1_f64),
            y: rng.gen_range(-0.1_f64..=0.1_f64),
        }
    };
    Affine {
        linear,
        translation,
    }
}

fn random_flame(num_functions: usize) -> Flame {
    random_flame_from_rules((0..num_functions).map(|_| (Affine::identity(), random_affine())))
}

fn random_flame_with_rules<I>(num_functions: usize, rules: I) -> Flame
where
    I: Iterator<Item = (Affine, Affine)>,
{
    let mut flame = random_flame(num_functions);
    let mut coeffs = [0.; variations::NUM];
    coeffs[0] = 1.;
    flame.functions.extend(rules.map(|(post, pre)| Function {
        pre,
        coeffs,
        color: random_color(&mut thread_rng()),
        post,
    }));
    flame
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    const DIM: usize = 800;
    const POINTS_PER_DIM: usize = 2_usize.pow(10);
    const NUM_SKIP: usize = 2_usize.pow(5);
    const NUM_TAKE: usize = 2_usize.pow(7);
    const GAMMA: f64 = 2.2;

    let mut rng = thread_rng();
    let mut sys = System::new("Flame", DIM, DIM);

    // Dimensions of the square that we're generating points in and sampling from.
    const ORIG_X: f64 = -2.;
    const ORIG_Y: f64 = -2.;
    const ORIG_DIM: f64 = 4.;

    let num_functions = rng.gen_range(1..=2_usize.pow(2));
    let mut flame = random_flame(num_functions);
    flame.global_transform = Affine::dilation(O, (DIM - 1) as f64 / ORIG_DIM)
        * Affine::translation(Vector::new(-ORIG_X, -ORIG_Y));

    let mut buf = {
        let buf = (0..DIM * DIM)
            .map(|_| {
                (
                    AtomicCounter::new(0),
                    AtomicCounter::new(0),
                    AtomicCounter::new(0),
                    AtomicCounter::new(0),
                )
            })
            .collect_vec();
        Array2::from_shape_vec((DIM, DIM), buf).unwrap_or_else(|_| unreachable!())
    };

    loop {
        println!("running flame");
        flame.run(POINTS_PER_DIM, NUM_SKIP, NUM_TAKE, &mut buf);
        println!("flame finished; rendering");

        // SAFETY ???
        let mut result: Array2<(Counter, Counter, Counter, Counter)> =
            unsafe { std::mem::transmute(buf) };
        let max = *result.iter().map(|(_, _, _, c)| c).max().unwrap() as f64;

        sys.draw_and_render(|gfx| {
            gfx.color = Color::black();
            gfx.clear();
            for ((i, j), &(r, g, b, c)) in result.indexed_iter() {
                if c == 0 {
                    continue;
                }
                let scale = (c as f64).log(max).powf(1. / GAMMA);
                let red = scale * (r as f64 / COLOR_RESOLUTION as f64 / c as f64);
                let green = scale * (g as f64 / COLOR_RESOLUTION as f64 / c as f64);
                let blue = scale * (b as f64 / COLOR_RESOLUTION as f64 / c as f64);
                gfx.color = Color::new(red, green, blue).unwrap();
                gfx.draw_point(Point::new(i as f64, j as f64));
            }
        });

        // Hash the flame to get a name for it.  Note that the hash includes the
        // "global transform"; i.e., the camera transform.  Whether this is
        // desirable or not is debatable.  It means that "the same flame" can go
        // by different names---which is bad---but it lets us automatically get
        // different filenames for pictures from different perspectives---which
        // is good.
        let name = {
            let mut name = String::new();
            for b in flame.hash() {
                write!(&mut name, "{:02x?}", b)?;
            }
            name
        };

        // Save the flame parameters
        let param_filename = {
            let mut path = PathBuf::new();
            path.push("flames");
            std::fs::create_dir_all(&path)?;
            path.push(&name);
            path.set_extension("yaml");
            path
        };
        let file = File::create(param_filename)?;
        serde_yaml::to_writer(file, &flame)?;

        // Write a screenshot.
        let screenshot = sys.screenshot();
        let screenshot_filename = {
            let mut path = dirs::picture_dir().unwrap();
            path.push("flames");
            std::fs::create_dir_all(&path)?;
            path.push(&name);
            path.set_extension("png");
            path
        };
        screenshot.save(screenshot_filename)?;

        // Let the user change the camera position.

        let p = sys.wait_click(MouseButton::Left);
        let q = sys.wait_click(MouseButton::Left);

        let diff = q - p;
        let scale = DIM as f64 / diff.x.abs().max(diff.y.abs());
        let dx = p.x.min(q.x);
        let dy = p.y.min(q.y);
        flame.global_transform = Affine::dilation(O, scale)
            * Affine::translation(Vector::new(dx, dy)).inverse()
            * flame.global_transform;

        result.fill((0, 0, 0, 0));
        // SAFETY ???
        buf = unsafe { std::mem::transmute(result) };
    }
}

lazy_static! {
    static ref SIERPINSKI_CARPET_RULES: [Affine; 8] = [
        Affine::translation(Vector::new(-2. / 3., -2. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(-2. / 3., 0.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(-2. / 3., 2. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(0., -2. / 3.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(0., 2. / 3.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(2. / 3., -2. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(2. / 3., 0.)) * Affine::dilation(Point::origin(), 1. / 3.),
        Affine::translation(Vector::new(2. / 3., 2. / 3.))
            * Affine::dilation(Point::origin(), 1. / 3.),
    ];
}
