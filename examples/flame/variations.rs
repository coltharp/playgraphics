#![allow(dead_code)]

use playgraphics::Affine;
use playgraphics::Point;
use rand::prelude::*;

// Aliases for readability

use std::f64::consts::PI as π;

const O: Point = Point::origin();

#[inline]
fn sqrt(z: f64) -> f64 {
    z.sqrt()
}

#[inline]
fn exp(z: f64) -> f64 {
    z.exp()
}

#[inline]
fn sin(θ: f64) -> f64 {
    θ.sin()
}

#[inline]
fn cos(θ: f64) -> f64 {
    θ.cos()
}

#[inline]
fn tan(θ: f64) -> f64 {
    θ.tan()
}

#[inline]
fn sinh(θ: f64) -> f64 {
    θ.sinh()
}

#[inline]
fn cosh(θ: f64) -> f64 {
    θ.cosh()
}

// Variations

fn linear(p: Point, _: Affine) -> Point {
    p
}

fn sinusoidal(p: Point, _: Affine) -> Point {
    p.map(sin)
}

fn spherical(p: Point, _: Affine) -> Point {
    p.map(|z| z / (p - O).norm_squared())
}

fn swirl(p @ Point { x, y }: Point, _: Affine) -> Point {
    let r2 = (p - O).norm_squared();
    Point {
        x: x * sin(r2) - y * cos(r2),
        y: x * cos(r2) + y * sin(r2),
    }
}

fn horshoe(p @ Point { x, y }: Point, _: Affine) -> Point {
    let r = (p - O).norm();
    Point {
        x: ((x - y) + (x + y)) / r,
        y: (2. * x + y) / r,
    }
}

fn polar(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    Point {
        x: θ / π,
        y: r - 1.,
    }
}

fn handkerchief(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    Point {
        x: r * sin(θ + r),
        y: r * cos(θ - r),
    }
}

fn heart(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    Point {
        x: r * sin(θ * r),
        y: -r * cos(θ * r),
    }
}

fn disc(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    Point {
        x: (θ / π) * sin(π * r),
        y: (θ / π) * cos(π * r),
    }
}

fn spiral(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    Point {
        x: (cos(θ) + sin(r)) / r,
        y: (sin(θ) - cos(r)) / r,
    }
}

fn hyperbolic(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    Point {
        x: sin(θ) / r,
        y: r * cos(θ),
    }
}

fn diamond(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    Point {
        x: sin(θ) * cos(r),
        y: cos(θ) * sin(r),
    }
}

fn ex(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    let p0 = sin(θ + r);
    let p1 = cos(θ - r);
    Point {
        x: r * (p0.powi(3) + p1.powi(3)),
        y: r * (p0.powi(3) - p1.powi(3)),
    }
}

fn julia(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    let &&Ω = &[0., π].choose(&mut rand::thread_rng()).unwrap();
    Point {
        x: sqrt(r) * cos(θ / 2. + Ω),
        y: sqrt(r) * sin(θ / 2. + Ω),
    }
}

fn bent(Point { x, y }: Point, _: Affine) -> Point {
    let (x, y) = if x >= 0. && y >= 0. {
        (x, y)
    } else if x < 0. && y >= 0. {
        (2. * x, y)
    } else if x >= 0. && y < 0. {
        (x, y / 2.)
    } else {
        (2. * x, y / 2.)
    };
    Point { x, y }
}

fn waves(Point { x, y }: Point, t: Affine) -> Point {
    let b = t.linear.elems[0][1];
    let c = t.translation.x;
    let e = t.linear.elems[1][1];
    let f = t.translation.y;
    Point {
        x: x + b * sin(y / c.powi(2)),
        y: y + e * sin(y / f.powi(2)),
    }
}

fn fisheye(p @ Point { x, y }: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    Point {
        x: 2. * y / (r + 1.),
        y: 2. * x / (r + 1.),
    }
}

fn popcorn(Point { x, y }: Point, t: Affine) -> Point {
    let c = t.translation.x;
    let f = t.translation.y;
    Point {
        x: x + c * tan(3. * y),
        y: y + f * sin(tan(3. * x)),
    }
}

fn exponential(Point { x, y }: Point, _: Affine) -> Point {
    let c = (x - 1.).exp();
    Point {
        x: c * cos(π * y),
        y: c * sin(π * y),
    }
}

fn power(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    let c = r.powf(sin(θ));
    Point {
        x: c * cos(θ),
        y: c * sin(θ),
    }
}

fn cosine(Point { x, y }: Point, _: Affine) -> Point {
    Point {
        x: cos(π * x) * cosh(y),
        y: -sin(π * x) * sinh(y),
    }
}

fn rings(p: Point, t: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    let c = t.translation.x;
    let m = (r + c.powi(2)) % (2. * c.powi(2)) - c.powi(2) + r * (1. - c.powi(2));
    Point {
        x: m * cos(θ),
        y: m * sin(θ),
    }
}

fn fan(p: Point, t: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let θ = v.angle();
    let c = t.translation.x;
    let f = t.translation.y;
    let t = π * c.powi(2);
    if (θ + f) % t > t / 2. {
        Point {
            x: r * cos(θ - t / 2.),
            y: sin(θ - t / 2.),
        }
    } else {
        Point {
            x: r * cos(θ + t / 2.),
            y: sin(θ + t / 2.),
        }
    }
}

fn eyefish(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r = v.norm();
    let c = 2. / (r + 1.);
    p.map(|z| c * z)
}

fn bubble(p: Point, _: Affine) -> Point {
    let v = p - O;
    let r2 = v.norm_squared();
    p.map(|z| 4. * z / (r2 + 4.))
}

fn cylinder(Point { x, y }: Point, _: Affine) -> Point {
    Point { x: sin(x), y }
}

fn noise(Point { x, y }: Point, _: Affine) -> Point {
    let Ψ1 = thread_rng().gen_range(0_f64..=1_f64);
    let Ψ2 = thread_rng().gen_range(0_f64..=2_f64 * π);
    Point {
        x: Ψ1 * x * cos(Ψ2),
        y: Ψ1 * y * sin(Ψ2),
    }
}

fn blur(_: Point, _: Affine) -> Point {
    let Ψ1 = thread_rng().gen_range(0_f64..=1_f64);
    let Ψ2 = thread_rng().gen_range(0_f64..=2_f64 * π);
    Point {
        x: Ψ1 * cos(Ψ2),
        y: Ψ1 * sin(Ψ2),
    }
}

fn gaussian(_: Point, _: Affine) -> Point {
    let c = (0..4)
        .map(|_| thread_rng().gen_range(-2_f64..=-1_f64))
        .sum::<f64>();
    let Ψ5 = thread_rng().gen_range(0_f64..=2_f64 * π);
    Point {
        x: c * cos(Ψ5),
        y: c * sin(Ψ5),
    }
}

fn tangent(Point { x, y }: Point, _: Affine) -> Point {
    Point {
        x: sin(x) / cos(y),
        y: tan(y),
    }
}

fn square(_: Point, _: Affine) -> Point {
    Point {
        x: thread_rng().gen_range(-0.5..=0.5),
        y: thread_rng().gen_range(-0.5..=0.5),
    }
}

fn cross(p @ Point { x, y }: Point, _: Affine) -> Point {
    let c = sqrt(1. / (x.powi(2) - y.powi(2)).powi(2));
    p.map(|z| c * z)
}

pub const NUM: usize = 23;
pub const ALL: [fn(Point, Affine) -> Point; NUM] = [
    linear,
    sinusoidal,
    spherical,
    swirl,
    horshoe,
    polar,
    handkerchief,
    heart,
    disc,
    spiral,
    hyperbolic,
    diamond,
    ex,
    julia,
    bent,
    fisheye,
    eyefish,
    bubble,
    cylinder,
    tangent,
    cross,
    popcorn,
    fan,
];
