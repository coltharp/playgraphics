use playgraphics::ifs;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use playgraphics::Vector;
use rayon::prelude::*;

const POINTS: usize = 2_usize.pow(5);
const ITERATIONS: usize = 2_usize.pow(15);
const DIM: usize = 800;

fn main() {
    let rules = [
        Affine::dilation(Point::origin(), 0.5),
        Affine::translation(Vector::new(0.5, 0.0)) * Affine::dilation(Point::origin(), 0.5),
        Affine::translation(Vector::new(0.0, 0.5)) * Affine::dilation(Point::origin(), 0.5),
        Affine::translation(Vector::new(0.5, 0.5)) * Affine::dilation(Point::origin(), 0.5),
    ];

    let inits = (0..POINTS)
        .into_par_iter()
        .map(|_| Point::new(rand::random(), rand::random()));
    let points = ifs::run_slice_par(&rules, inits)
        .flat_map_iter(|it| it.take(ITERATIONS))
        .map(|u| u.1)
        .collect::<Vec<_>>();

    let mut sys = System::new("IFS Square", DIM, DIM);
    let t = Affine::dilation(Point::origin(), DIM as f64);

    sys.draw_and_render(|g| {
        g.color = Color::white();
        g.clear();
        g.color = Color::black();
        for p in points {
            g.draw_point(t * p);
        }
    });
    sys.park();
}
