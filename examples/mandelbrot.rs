use ndarray::Array2;
use num::Complex;
use num::Zero;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use rayon::prelude::*;

fn mandelbrot(iterations: usize, points: &Array2<Complex<f64>>) -> Array2<Option<usize>> {
    let values = points
        .par_iter()
        .map(|c| {
            let mut z = Complex::<f64>::zero();
            for i in 0..iterations {
                if z.norm() >= 2. {
                    return Some(i);
                }
                z = z.powi(2) + c;
            }
            None
        })
        .collect::<Vec<_>>();
    Array2::from_shape_vec(points.raw_dim(), values).unwrap()
}

fn main() {
    const DIM: usize = 800;
    const ITERATIONS: usize = 128;

    let points = Array2::from_shape_fn((DIM, DIM), |(i, j)| Complex {
        re: (4. * i as f64 / DIM as f64) - 2.,
        im: (4. * j as f64 / DIM as f64) - 2.,
    });
    let values = mandelbrot(ITERATIONS, &points);
    let max = *values.iter().flatten().max().unwrap() as f64;
    let mut sys = System::new("Mandelbrot", DIM, DIM);
    sys.draw_and_render(|g| {
        g.color = Color::black();
        g.clear();
        values.indexed_iter().for_each(|((i, j), v)| {
            if let &Some(v) = v {
                let v = v as f64;
                let c = if v == 0. { 0. } else { v.log(max) };
                g.color = Color::gray(c).unwrap();
                g.draw_point(Point::new(i as f64, j as f64));
            }
        })
    });
    sys.park();
}
