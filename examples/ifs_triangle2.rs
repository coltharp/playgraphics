use itertools::Itertools;
use playgraphics::ifs;
use playgraphics::Affine;
use playgraphics::Color;
use playgraphics::Point;
use playgraphics::System;
use rand::prelude::*;
use rayon::prelude::*;

use std::f64::consts::PI;
const O: Point = Point::origin();
const NUM_POINTS: usize = 2_usize.pow(6);
const NUM_SKIP: usize = 2_usize.pow(5);
const NUM_TAKE: usize = 2_usize.pow(15);
const DIM: usize = 800;

fn main() {
    let rules: [Affine; 4] = [
        [Affine::dilation(O, 0.5)].iter().rev().product(),
        [Affine::dilation(Point::new(0., 1.), 0.5)]
            .iter()
            .rev()
            .product(),
        [Affine::dilation(Point::new(1., 0.), 0.5)]
            .iter()
            .rev()
            .product(),
        [
            Affine::dilation(O, 0.5),
            Affine::rotation(Point::new(1. / 4., 1. / 4.), PI),
        ]
        .iter()
        .rev()
        .product(),
    ];
    let rules = playgraphics::util::carry_indices(rules.into_iter()).collect_vec();

    let inits = (0..NUM_POINTS)
        .into_par_iter()
        .map(|_| Point::new((random::<f64>() - 0.5).abs(), (random::<f64>() - 0.5).abs()));
    let results = ifs::run_slice_par(&rules, inits)
        .flat_map_iter(|it| it.skip(NUM_SKIP).take(NUM_TAKE))
        .collect::<Vec<_>>();

    let colors = (0..4)
        .flat_map(|_| Color::new(random(), random(), random()))
        .collect_vec();

    let mut sys = System::new("IFS Triangle 2", 800, 800);
    sys.draw_and_render(|g| {
        g.transform = Affine::dilation(O, DIM as f64);
        g.color = Color::white();
        g.clear();
        for (f, p) in results {
            let color = colors[f.value];
            let old_color = match g.pixel(p) {
                Some(p) => p,
                None => continue,
            };
            let new_color = color.average(old_color);
            g.color = new_color;
            g.draw_point(p);
        }
    });
    sys.park();
}
