;;; Directory Local Variables            -*- no-byte-compile: t -*-
;;; For more information see (info "(emacs) Directory Variables")

((rustic-mode . ((eglot-workspace-configuration . (:rust-analyzer
                                                   (:cargo
                                                    (:features "all")
                                                    :check
                                                    (:command "clippy")))))))
