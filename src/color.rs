use serde::Deserialize;
use serde::Serialize;

#[derive(PartialEq, PartialOrd, Clone, Copy, Default, Debug, Serialize, Deserialize)]
pub struct Color {
    red: f64,
    green: f64,
    blue: f64,
}

impl Color {
    #[inline]
    pub fn new(red: f64, green: f64, blue: f64) -> Option<Color> {
        if (0. ..=1.).contains(&red) && (0. ..=1.).contains(&green) && (0. ..=1.).contains(&blue) {
            Some(Color { red, green, blue })
        } else {
            None
        }
    }

    #[inline]
    pub const fn black() -> Color {
        Color {
            red: 0.,
            green: 0.,
            blue: 0.,
        }
    }

    #[inline]
    pub const fn red() -> Color {
        Color {
            red: 1.,
            green: 0.,
            blue: 0.,
        }
    }

    #[inline]
    pub const fn green() -> Color {
        Color {
            red: 0.,
            green: 1.,
            blue: 0.,
        }
    }

    #[inline]
    pub const fn blue() -> Color {
        Color {
            red: 0.,
            green: 0.,
            blue: 1.,
        }
    }

    #[inline]
    pub const fn cyan() -> Color {
        Color {
            red: 0.,
            green: 1.,
            blue: 1.,
        }
    }

    #[inline]
    pub const fn magenta() -> Color {
        Color {
            red: 1.,
            green: 0.,
            blue: 1.,
        }
    }

    #[inline]
    pub const fn yellow() -> Color {
        Color {
            red: 0.,
            green: 1.,
            blue: 1.,
        }
    }

    #[inline]
    pub const fn white() -> Color {
        Color {
            red: 1.,
            green: 1.,
            blue: 1.,
        }
    }

    #[inline]
    pub fn gray(value: f64) -> Option<Color> {
        Color::new(value, value, value)
    }

    #[inline]
    pub fn average(self, other: Color) -> Color {
        Color {
            red: (self.red + other.red) / 2.,
            green: (self.green + other.green) / 2.,
            blue: (self.blue + other.blue) / 2.,
        }
    }

    #[inline]
    pub fn interpolate(self, other: Color, t: f64) -> Color {
        Color {
            red: (1. - t) * self.red + t * other.red,
            green: (1. - t) * self.green + t * other.green,
            blue: (1. - t) * self.blue + t * other.blue,
        }
    }

    #[inline]
    pub fn r(self) -> f64 {
        self.red
    }

    #[inline]
    pub fn g(self) -> f64 {
        self.green
    }

    #[inline]
    pub fn b(self) -> f64 {
        self.blue
    }

    #[inline]
    pub fn opposite(self) -> Color {
        Color {
            red: 1. - self.red,
            green: 1. - self.green,
            blue: 1. - self.blue,
        }
    }

    #[inline]
    pub fn from_0rgb(x: u32) -> Color {
        let red = ((x >> 16) as u8) as f64 / u8::MAX as f64;
        let green = ((x >> 8) as u8) as f64 / u8::MAX as f64;
        let blue = (x as u8) as f64 / u8::MAX as f64;
        Color { red, green, blue }
    }

    #[inline]
    pub fn to_0rgb(self) -> u32 {
        let r = (self.red * u8::MAX as f64).round() as u8;
        let g = (self.green * u8::MAX as f64).round() as u8;
        let b = (self.blue * u8::MAX as f64).round() as u8;
        ((r as u32) << 16) | ((g as u32) << 8) | b as u32
    }
}

impl From<(u8, u8, u8)> for Color {
    fn from((red, green, blue): (u8, u8, u8)) -> Self {
        Color {
            red: red as f64 / u8::MAX as f64,
            green: green as f64 / u8::MAX as f64,
            blue: blue as f64 / u8::MAX as f64,
        }
    }
}
