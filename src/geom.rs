use core_extensions::impl_call;
use num::Complex;
use num::One;
use num::Zero;
use serde::Deserialize;
use serde::Serialize;
use std::fmt;
use std::fmt::Display;
use std::fmt::Formatter;
use std::iter::Product;
use std::iter::Sum;
use std::ops::Add;
use std::ops::AddAssign;
use std::ops::Div;
use std::ops::DivAssign;
use std::ops::Mul;
use std::ops::MulAssign;
use std::ops::Neg;
use std::ops::Sub;
use std::ops::SubAssign;

macro_rules! impl_zero {
    (|| -> $Self:ty $body:block) => {
        impl Zero for $Self {
            #[inline]
            fn zero() -> $Self $body

            #[inline]
            fn is_zero(&self) -> bool {
                *self == Self::zero()
            }
        }
    };
}

macro_rules! impl_unop {
    (
        impl $Trait:ident for $Self:ty {
            fn $fn:ident($self:ident) -> $Output:ty $body:block
        }
    ) => {
        impl $Trait for $Self {
            type Output = $Output;
            #[inline]
            fn $fn($self) -> $Output $body
        }
        impl $Trait for &$Self {
            type Output = $Output;
            #[inline]
            fn $fn($self) -> $Output $body
        }
    }
}

macro_rules! impl_neg {
    (|$self:ident: $Self:ty| -> $Output:ty $body:block) => {
        impl_unop! {
            impl Neg for $Self {
                fn neg($self) -> $Output $body
            }
        }
    };
}

/// Implement a binop trait for all combinations of value and reference
/// simultaneously.
macro_rules! impl_binop {
    (
        impl $Trait:ident <$Rhs:ty> for $Self:ty {
            fn $fn:ident($self:ident, $rhs:ident) -> $Output:ty $body:block
        }
    ) => {
        impl $Trait<$Rhs> for $Self {
            type Output = $Output;
            #[inline]
            fn $fn($self, $rhs: $Rhs) -> $Output $body
        }
        impl $Trait<$Rhs> for &$Self {
            type Output = $Output;
            #[inline]
            fn $fn($self, $rhs: $Rhs) -> $Output $body
        }
        impl $Trait<&$Rhs> for $Self {
            type Output = $Output;
            #[inline]
            fn $fn($self, $rhs: &$Rhs) -> $Output $body
        }
        impl $Trait<&$Rhs> for &$Self {
            type Output = $Output;
            #[inline]
            fn $fn($self, $rhs: &$Rhs) -> $Output $body
        }
    }
}

macro_rules! mk_impl_binop {
    ($name:ident, $Trait:ident, $fn:ident) => {
        macro_rules! $name {
            (|$self:ident: $Self:ty, $rhs:ident: $Rhs:ty| -> $Output:ty $body:block) => {
                impl_binop! {
                    impl $Trait<$Rhs> for $Self {
                        fn $fn($self, $rhs) -> $Output $body
                    }
                }
            };
        }
    };
}

mk_impl_binop!(impl_add, Add, add);
mk_impl_binop!(impl_sub, Sub, sub);
mk_impl_binop!(impl_mul, Mul, mul);
mk_impl_binop!(impl_div, Div, div);

macro_rules! impl_binop_assign {
    (
        impl $Trait:ident<$Rhs:ty> for $Self:ty {
            fn $fn:ident($self:ident, $rhs:ident) $body:block
        }
    ) => {
        impl $Trait<$Rhs> for $Self {
            #[inline]
            fn $fn(&mut $self, $rhs: $Rhs) $body
        }
        impl $Trait<&$Rhs> for $Self {
            #[inline]
            fn $fn(&mut $self, $rhs: &$Rhs) $body
        }
    }
}

macro_rules! mk_impl_binop_assign {
    ($name:ident, $Trait:ident, $fn:ident) => {
        macro_rules! $name {
            (|$self:ident: $Self:ty, $rhs:ident: $Rhs:ty| $body:block) => {
                impl_binop_assign! {
                    impl $Trait<$Rhs> for $Self {
                        fn $fn($self, $rhs) $body
                    }
                }
            };
        }
    };
}

mk_impl_binop_assign!(impl_add_assign, AddAssign, add_assign);
mk_impl_binop_assign!(impl_sub_assign, SubAssign, sub_assign);
mk_impl_binop_assign!(impl_mul_assign, MulAssign, mul_assign);
mk_impl_binop_assign!(impl_div_assign, DivAssign, div_assign);

macro_rules! impl_sumprod {
    (
        impl $Trait:ident for $Self:ty {
            fn $fn:ident($iter:ident) $body:block
    }) => {
        impl $Trait for $Self {
            #[inline]
            fn $fn<I: Iterator<Item = $Self>>($iter: I) -> Self $body
        }
        impl<'a> $Trait<&'a Self> for $Self {
            #[inline]
            fn $fn<I: Iterator<Item = &'a $Self>>($iter: I) -> Self $body
        }
    }
}

macro_rules! impl_sum(($Self:ty) => {
    impl_sumprod! {
        impl Sum for $Self {
            fn sum(iter) {
                iter.fold(Self::zero(), |x, y| x + y)
            }
        }
    }
});

macro_rules! impl_product(($Self:ty) => {
    impl_sumprod! {
        impl Product for $Self {
            fn product(iter) {
                iter.fold(Self::one(), |x, y| x * y)
            }
        }
    }
});

/// Vectors in a two-dimensional linear space over the “field” [f64].
///
/// The [Default] vector is the zero vector.
#[derive(PartialEq, PartialOrd, Clone, Copy, Default, Debug, Serialize, Deserialize)]
pub struct Vector {
    pub x: f64,
    pub y: f64,
}

impl Vector {
    #[inline]
    pub const fn new(x: f64, y: f64) -> Vector {
        Vector { x, y }
    }

    #[inline]
    pub const fn repeat(a: f64) -> Vector {
        Vector { x: a, y: a }
    }

    #[inline]
    pub fn norm_squared(self) -> f64 {
        self.x.powi(2) + self.y.powi(2)
    }

    #[inline]
    pub fn norm(self) -> f64 {
        self.norm_squared().sqrt()
    }

    #[inline]
    pub fn distance(self, other: Vector) -> f64 {
        (self - other).norm()
    }

    #[inline]
    pub fn angle(self) -> f64 {
        self.y.atan2(self.x)
    }
}

impl Display for Vector {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "<{},{}>", self.x, self.y)
    }
}

impl_zero! {
    || -> Vector { Vector::repeat(0.) }
}

impl_add! {
    |self: Vector, rhs: Vector| -> Vector {
        Vector {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl_add_assign! {
    |self: Vector, rhs: Vector| {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl_sum!(Vector);

impl_sub! {
    |self: Vector, rhs: Vector| -> Vector {
        Vector {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl_neg! {
    |self: Vector| -> Vector {
        Vector {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl_sub_assign! {
    |self: Vector, rhs: Vector| {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl_mul! {
    |self: f64, rhs: Vector| -> Vector {
        Vector {
            x: self * rhs.x,
            y: self * rhs.y,
        }
    }
}

impl_mul_assign! {
    |self: Vector, rhs: f64| {
        self.x *= rhs;
        self.y *= rhs;
    }
}

impl_div! {
    |self: Vector, rhs: f64| -> Vector {
        Vector {
            x: self.x / rhs,
            y: self.y / rhs,
        }
    }
}

impl_div_assign! {
    |self: Vector, rhs: f64| {
        self.x /= rhs;
        self.y /= rhs;
    }
}

/// Linear transformations from [Vector] to [Vector].
///
/// Linear transformations act on vectors via multiplication, and they are
/// composed via multiplication with each other.  The semantics of `MulAssign`
/// are that `A *= B` is equivalent to `A = A * B`.
///
/// Linear transformations can also be added and subtracted, as they are
/// elements of GL2(R) (for sufficient values of R).
///
/// The [Default] linear transformation is the identity.
#[derive(PartialEq, PartialOrd, Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Linear {
    pub elems: [[f64; 2]; 2],
}

impl Linear {
    #[inline]
    pub const fn identity() -> Linear {
        Linear {
            elems: [[1., 0.], [0., 1.]],
        }
    }

    #[inline]
    pub const fn repeat(elem: f64) -> Linear {
        Linear {
            elems: [[elem; 2]; 2],
        }
    }

    #[inline]
    pub fn rotation(angle: f64) -> Linear {
        Linear {
            elems: [[angle.cos(), -angle.sin()], [angle.sin(), angle.cos()]],
        }
    }

    #[inline]
    pub const fn dilation_x(scale: f64) -> Linear {
        Linear {
            elems: [[scale, 0.], [0., 1.]],
        }
    }

    #[inline]
    pub const fn dilation_y(scale: f64) -> Linear {
        Linear {
            elems: [[1., 0.], [0., scale]],
        }
    }

    #[inline]
    pub const fn dilation(scale: f64) -> Linear {
        Linear {
            elems: [[scale, 0.], [0., scale]],
        }
    }

    #[inline]
    pub const fn reflection_x() -> Linear {
        Linear {
            elems: [[-1., 0.], [0., 1.]],
        }
    }

    #[inline]
    pub const fn reflection_y() -> Linear {
        Linear {
            elems: [[1., 0.], [0., -1.]],
        }
    }

    #[inline]
    pub fn shear_x(m: f64) -> Linear {
        Linear {
            elems: [[1., m], [0., 1.]]
        }
    }

    #[inline]
    pub fn shear_y(m: f64) -> Linear {
        Linear {
            elems: [[1., 0.], [m, 1.]]
        }
    }

    #[inline]
    pub fn trace(self) -> f64 {
        self.elems[0][0] + self.elems[1][1]
    }

    #[inline]
    pub fn determinant(self) -> f64 {
        self.elems[0][0] * self.elems[1][1] - self.elems[0][1] * self.elems[1][0]
    }

    #[inline]
    pub fn inverse(self) -> Linear {
        1. / self.determinant() * (Linear::from(self.trace()) - self)
    }
}

impl_zero! {
    || -> Linear {
        Linear::repeat(0.)
    }
}

impl One for Linear {
    #[inline]
    fn one() -> Linear {
        Linear::identity()
    }
}

impl Default for Linear {
    #[inline]
    fn default() -> Linear {
        Linear::one()
    }
}

impl From<f64> for Linear {
    #[inline]
    fn from(elem: f64) -> Linear {
        elem * Linear::identity()
    }
}

impl From<[[f64; 2]; 2]> for Linear {
    #[inline]
    fn from(elems: [[f64; 2]; 2]) -> Linear {
        Linear { elems }
    }
}

impl_add! {
    |self: Linear, rhs: Linear| -> Linear {
        Linear {
            elems: [
                [
                    self.elems[0][0] + rhs.elems[0][0],
                    self.elems[0][1] + rhs.elems[0][1],
                ],
                [
                    self.elems[1][0] + rhs.elems[1][0],
                    self.elems[1][1] + rhs.elems[1][1],
                ],
            ],
        }
    }
}

impl_add_assign! {
    |self: Linear, rhs: Linear| {
        self.elems[0][0] += rhs.elems[0][0];
        self.elems[0][1] += rhs.elems[0][1];
        self.elems[1][0] += rhs.elems[1][0];
        self.elems[1][1] += rhs.elems[1][1];
    }
}

impl_sum!(Linear);

impl_sub! {
    |self: Linear, rhs: Linear| -> Linear {
        Linear {
            elems: [
                [
                    self.elems[0][0] - rhs.elems[0][0],
                    self.elems[0][1] - rhs.elems[0][1],
                ],
                [
                    self.elems[1][0] - rhs.elems[1][0],
                    self.elems[1][1] - rhs.elems[1][1],
                ],
            ],
        }
    }
}

impl_sub_assign! {
    |self: Linear, rhs: Linear| {
        self.elems[0][0] -= rhs.elems[0][0];
        self.elems[0][1] -= rhs.elems[0][1];
        self.elems[1][0] -= rhs.elems[1][0];
        self.elems[1][1] -= rhs.elems[1][1];
    }
}

impl_neg! {
    |self: Linear| -> Linear {
        Linear {
            elems: [
                [-self.elems[0][0], -self.elems[0][1]],
                [-self.elems[1][0], -self.elems[1][1]]
            ]
        }
    }
}

impl_mul! {
    |self: f64, rhs: Linear| -> Linear {
        Linear {
            elems: [
                [self * rhs.elems[0][0], self * rhs.elems[0][1]],
                [self * rhs.elems[1][0], self * rhs.elems[1][1]],
            ]
        }
    }
}

impl_mul_assign! {
    |self: Linear, rhs: f64| {
        self.elems[0][0] *= rhs;
        self.elems[0][1] *= rhs;
        self.elems[1][0] *= rhs;
        self.elems[1][1] *= rhs;
    }
}

impl_div! {
    |self: Linear, rhs: f64| -> Linear {
        Linear {
            elems: [
                [self.elems[0][0] / rhs, self.elems[0][1] / rhs],
                [self.elems[1][0] / rhs, self.elems[1][1] / rhs],
            ],
        }
    }
}

impl_div_assign! {
    |self: Linear, rhs: f64| {
        self.elems[0][0] /= rhs;
        self.elems[0][1] /= rhs;
        self.elems[1][0] /= rhs;
        self.elems[1][1] /= rhs;
    }
}

impl_mul! {
    |self: Linear, rhs: Vector| -> Vector {
        Vector {
            x: rhs.x * self.elems[0][0] + rhs.y * self.elems[0][1],
            y: rhs.x * self.elems[1][0] + rhs.y * self.elems[1][1],
        }
    }
}

impl_mul_assign! {
    |self: Vector, rhs: Linear| {
        self.x = self.x * rhs.elems[0][0] + self.y * rhs.elems[0][1];
        self.y = self.y * rhs.elems[1][0] + self.y * rhs.elems[0][1];
    }
}

impl_mul! {
    |self: Linear, rhs: Linear| -> Linear {
        Linear {
            elems: [
                [
                    self.elems[0][0] * rhs.elems[0][0] + self.elems[0][1] * rhs.elems[1][0],
                    self.elems[0][0] * rhs.elems[0][1] + self.elems[0][1] * rhs.elems[1][1],
                ],
                [
                    self.elems[1][0] * rhs.elems[0][0] + self.elems[1][1] * rhs.elems[1][0],
                    self.elems[1][0] * rhs.elems[0][1] + self.elems[1][1] * rhs.elems[1][1],
                ],
            ],
        }
    }
}

impl_call! {
    fn ref_call(self: Linear, rhs: Linear) -> Linear {
        self * rhs
    }
}

impl_call! {
    fn ref_call(self: Linear, rhs: &Linear) -> Linear {
        self * rhs
    }
}

impl_call! {
    fn ref_call(self: &Linear, rhs: Linear) -> Linear {
        *self * rhs
    }
}

impl_call! {
    fn ref_call(self: &Linear, rhs: &Linear) -> Linear {
        *self * rhs
    }
}

impl_call! {
    fn ref_call(self: Linear, rhs: Vector) -> Vector {
        self * rhs
    }
}

impl_call! {
    fn ref_call(self: Linear, rhs: &Vector) -> Vector {
        self * rhs
    }
}

impl_call! {
    fn ref_call(self: &Linear, rhs: Vector) -> Vector {
        *self * rhs
    }
}

impl_call! {
    fn ref_call(self: &Linear, rhs: &Vector) -> Vector {
        *self * rhs
    }
}

#[test]
fn test_mul_linear_linear() {
    let a = Linear {
        elems: [[1., 2.], [3., 4.]],
    };
    let b = Linear {
        elems: [[2., 0.], [1., 2.]],
    };
    let c = Linear {
        elems: [[4., 4.], [10., 8.]],
    };
    let d = Linear {
        elems: [[2., 4.], [7., 10.]],
    };
    assert_eq!(a * b, c);
    assert_eq!(b * a, d);
}

impl_mul_assign! {
    |self: Linear, rhs: Linear| {
        self.elems[0][0] = self.elems[0][0] * rhs.elems[0][0] + self.elems[0][1] * rhs.elems[1][0];
        self.elems[0][1] = self.elems[0][0] * rhs.elems[0][1] + self.elems[0][1] * rhs.elems[1][1];
        self.elems[1][0] = self.elems[1][0] * rhs.elems[0][0] + self.elems[1][1] * rhs.elems[1][0];
        self.elems[1][1] = self.elems[1][0] * rhs.elems[0][1] + self.elems[1][1] * rhs.elems[1][1];
    }
}

impl_product!(Linear);

/// Points in an affine space over the vector space [Vector].
///
/// Points can be subtracted from each other to get a difference, which is of
/// type [Vector].  Points can be translated by adding or subtracting vectors.
/// Points cannot be [Mul]tiplied by scalars.
#[derive(PartialEq, PartialOrd, Clone, Copy, Default, Debug, Serialize, Deserialize)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

pub mod point {
    pub const ORIGIN: super::Point = super::Point { x: 0., y: 0. };
}

impl Point {
    #[inline]
    pub const fn new(x: f64, y: f64) -> Point {
        Point { x, y }
    }

    #[inline]
    pub const fn origin() -> Point {
        point::ORIGIN
    }

    #[inline]
    pub fn bimap<F, G>(self, f: F, g: G) -> Point
    where
        F: FnOnce(f64) -> f64,
        G: FnOnce(f64) -> f64,
    {
        Point {
            x: f(self.x),
            y: g(self.y),
        }
    }

    #[inline]
    pub fn map_x<F>(self, f: F) -> Point
    where
        F: Fn(f64) -> f64,
    {
        self.bimap(f, |y| y)
    }

    #[inline]
    pub fn map_y<F>(self, f: F) -> Point
    where
        F: Fn(f64) -> f64,
    {
        self.bimap(|x| x, f)
    }

    #[inline]
    pub fn map<F>(self, f: F) -> Point
    where
        F: Fn(f64) -> f64,
    {
        Point {
            x: f(self.x),
            y: f(self.y),
        }
    }

    #[inline]
    pub fn trans_x(self, dx: f64) -> Point {
        self.map_x(|x| x + dx)
    }

    #[inline]
    pub fn trans_y(self, dy: f64) -> Point {
        self.map_y(|y| y + dy)
    }

    #[inline]
    pub fn angle_from(self, center: Point, other: Point) -> f64 {
        (self - center).angle() - (other - center).angle()
    }

    #[inline]
    pub fn midpoint(self, other: Point) -> Point {
        // This may seem like a strange formulation, but it compiles to
        // efficient and accurate code.  It's functionally the same as the thing
        // that we *want* to do---add the two points' coordinates and divide by
        // 2---but can't, because points aren't vectors and can't be added.
        Point::origin() + ((self - Point::origin()) + (other - Point::origin())) / 2.
    }

    #[inline]
    pub fn distance(self, other: Point) -> f64 {
        (other - self).norm()
    }
}

impl From<Complex<f64>> for Point {
    fn from(value: Complex<f64>) -> Self {
        Point {
            x: value.re,
            y: value.im,
        }
    }
}

impl From<Point> for Complex<f64> {
    fn from(value: Point) -> Self {
        Complex {
            re: value.x,
            im: value.y,
        }
    }
}

impl Display for Point {
    #[inline]
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "({},{})", self.x, self.y)
    }
}

impl_sub! {
    |self: Point, rhs: Point| -> Vector {
        Vector {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl_add! {
    |self: Point, rhs: Vector| -> Point {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl_add_assign! {
    |self: Point, rhs: Vector| {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl_sub! {
    |self: Point, rhs: Vector| -> Point {
        Point {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl_sub_assign! {
    |self: Point, rhs: Vector| {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

/// Affine transformations in two dimensions.
///
/// Affine transformations act on points via multiplication, and they are
/// composed via multiplication with each other.  The semantics of `MulAssign`
/// are that `A *= B` is equivalent to `A = A * B`.
///
/// The [Default] affine transformation is the identity.
///
/// [Vector]s can be converted to affine transformations via [from] or [into];
/// the linear component will be zero.  Likewise, [Linear] transformations can
/// be converted to affine transformations; the translation component will be
/// zero.
#[derive(PartialEq, PartialOrd, Clone, Copy, Debug, Serialize, Deserialize)]
pub struct Affine {
    pub linear: Linear,
    pub translation: Vector,
}

impl Affine {
    #[inline]
    pub fn identity() -> Affine {
        Affine {
            linear: Linear::one(),
            translation: Vector::zero(),
        }
    }

    #[inline]
    pub fn inverse(self) -> Affine {
        Affine {
            linear: self.linear.inverse(),
            translation: -self.linear.inverse() * self.translation,
        }
    }

    #[inline]
    pub fn conjugate_by(self, other: Affine) -> Affine {
        other.inverse() * self * other
    }

    #[inline]
    pub const fn translation_x(dx: f64) -> Affine {
        Affine::translation(Vector::new(dx, 0.))
    }

    #[inline]
    pub const fn translation_y(dy: f64) -> Affine {
        Affine::translation(Vector::new(0., dy))
    }

    #[inline]
    pub const fn translation(translation: Vector) -> Affine {
        Affine {
            linear: Linear::identity(),
            translation,
        }
    }

    #[inline]
    pub fn rotation(u: Point, angle: f64) -> Affine {
        Affine::from(Linear::rotation(angle)).conjugate_by((Point::origin() - u).into())
    }

    #[inline]
    pub fn dilation_x(u: Point, scale: f64) -> Affine {
        Affine::from(Linear::dilation_x(scale))
            .conjugate_by(Affine::translation(Point::origin() - u))
    }

    #[inline]
    pub fn dilation_y(u: Point, scale: f64) -> Affine {
        Affine::from(Linear::dilation_y(scale))
            .conjugate_by(Affine::translation(Point::origin() - u))
    }

    #[inline]
    pub fn dilation(u: Point, scale: f64) -> Affine {
        Affine::from(Linear::dilation(scale)).conjugate_by((Point::origin() - u).into())
    }

    #[inline]
    pub fn reflection_x(x: f64) -> Affine {
        Affine::from(Linear::reflection_x())
            .conjugate_by((Point::origin() - Point { x, y: 0. }).into())
    }

    #[inline]
    pub fn reflection_y(y: f64) -> Affine {
        Affine::from(Linear::reflection_y())
            .conjugate_by((Point::origin() - Point { x: 0., y }).into())
    }

    #[inline]
    pub fn shear_x(u: Point, m: f64) -> Affine {
        Affine::from(Linear::shear_x(m))
            .conjugate_by(Affine::translation(Point::origin() - u))
    }

    #[inline]
    pub fn shear_y(u: Point, m: f64) -> Affine {
        Affine::from(Linear::shear_y(m))
            .conjugate_by(Affine::translation(Point::origin() - u))
    }
}

impl From<Vector> for Affine {
    #[inline]
    fn from(t: Vector) -> Affine {
        Affine::translation(t)
    }
}

impl From<Linear> for Affine {
    #[inline]
    fn from(linear: Linear) -> Affine {
        Affine {
            linear,
            translation: Vector::zero(),
        }
    }
}

impl One for Affine {
    #[inline]
    fn one() -> Affine {
        Affine::identity()
    }
}

impl Default for Affine {
    #[inline]
    fn default() -> Affine {
        Affine::one()
    }
}

impl_mul! {
    |self: Affine, rhs: Point| -> Point {
        Point::origin() + (self.linear * (rhs - Point::origin())) + self.translation
    }
}

#[test]
fn test_mul_affine_point() {
    let a = Affine {
        linear: Linear::from([[0., -1.], [1., 0.]]),
        translation: Vector { x: 0., y: 0. },
    };
    let b = Point { x: 1., y: 2. };
    let c = Point { x: -2., y: 1. };
    assert_eq!(a * b, c);

    let d = Affine {
        linear: Linear::from([[-9., 3.], [3., -8.]]),
        translation: Vector { x: -3., y: 3. },
    };
    let e = Point { x: -3., y: -5. };
    let f = Point { x: 9., y: 34. };
    assert_eq!(d * e, f);
}

impl_mul_assign! {
    |self: Point, rhs: Affine| {
        *self = rhs * *self;
    }
}

impl_mul! {
    |self: Affine, rhs: Affine| -> Affine {
        Affine {
            linear: self.linear * rhs.linear,
            translation: self.translation + self.linear * rhs.translation
        }
    }
}

impl_mul_assign! {
    |self: Affine, rhs: Affine| {
        self.linear *= rhs.linear;
        self.translation += self.linear * rhs.translation;
    }
}

impl_product!(Affine);

impl_call! {
    fn ref_call(self: Affine, rhs: Affine) -> Affine {
        self * rhs
    }
}

impl_call! {
    fn ref_call(self: Affine, rhs: &Affine) -> Affine {
        self * rhs
    }
}

impl_call! {
    fn ref_call(self: &Affine, rhs: Affine) -> Affine {
        *self * rhs
    }
}

impl_call! {
    fn ref_call(self: &Affine, rhs: &Affine) -> Affine {
        *self * rhs
    }
}

impl_call! {
    fn ref_call(self: Affine, rhs: Point) -> Point {
        self * rhs
    }
}

impl_call! {
    fn ref_call(self: Affine, rhs: &Point) -> Point {
        self * rhs
    }
}

impl_call! {
    fn ref_call(self: &Affine, rhs: Point) -> Point {
        *self * rhs
    }
}

impl_call! {
    fn ref_call(self: &Affine, rhs: &Point) -> Point {
        *self * rhs
    }
}
