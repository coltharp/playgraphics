use crate::Color;
use crate::Point;
use rand::distributions::Distribution;
use rand::prelude::*;
use rand_distr::Normal;

pub type Paint = Box<dyn Fn(Point, Color) -> Color>;

pub fn color(color: Color) -> Paint {
    Box::new(move |_, _| color)
}

pub fn noise(base: Color, dev: f64) -> Paint {
    let r = Normal::new(base.r(), dev)
        .unwrap()
        .map(|f| f.max(0.).min(1.));
    let g = Normal::new(base.g(), dev)
        .unwrap()
        .map(|f| f.max(0.).min(1.));
    let b = Normal::new(base.b(), dev)
        .unwrap()
        .map(|f| f.max(0.).min(1.));
    Box::new(move |_, _| {
        let mut rng = thread_rng();
        Color::new(r.sample(&mut rng), g.sample(&mut rng), b.sample(&mut rng)).unwrap()
    })
}

pub fn intensity_noise(base: Color, dev: f64) -> Paint {
    let i = Normal::new(0., dev).unwrap().map(|f| f.max(0.).min(1.));
    Box::new(move |_, _| {
        let x = i.sample(&mut thread_rng());
        let r = (base.r() + x).max(0.).min(1.);
        let g = (base.g() + x).max(0.).min(1.);
        let b = (base.b() + x).max(0.).min(1.);
        Color::new(r, g, b).unwrap()
    })
}

pub fn linear_gradient_x(start_x: f64, start_color: Color, end_x: f64, end_color: Color) -> Paint {
    Box::new(move |p, _| {
        let t = (p.x - start_x) / (end_x - start_x);
        start_color.interpolate(end_color, t)
    })
}

pub fn linear_gradient_y(start_y: f64, start_color: Color, end_y: f64, end_color: Color) -> Paint {
    Box::new(move |p, _| {
        let t = (p.y - start_y) / (end_y - start_y);
        start_color.interpolate(end_color, t)
    })
}

pub fn radial_gradient(
    center: Point,
    center_color: Color,
    end_distance: f64,
    end_color: Color,
) -> Paint {
    Box::new(move |p, _| {
        let t = p.distance(center) / end_distance;
        center_color.interpolate(end_color, t)
    })
}
