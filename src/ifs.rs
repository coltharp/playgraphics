use crate::Point;
use core_extensions::callable::*;
use rand::prelude::*;
use rayon::prelude::*;

pub fn run<D, F>(distr: &D, init: Point) -> impl Iterator<Item = (F, Point)> + '_
where
    D: Distribution<F>,
    F: CallRef<Point, Returns = Point>,
{
    itertools::unfold(init, |p| {
        let f = distr.sample(&mut thread_rng());
        *p = f.ref_call(*p);
        Some((f, *p))
    })
}

pub fn run_move<D, F>(distr: D, init: Point) -> impl Iterator<Item = (F, Point)>
where
    D: Distribution<F>,
    F: CallRef<Point, Returns = Point>,
{
    itertools::unfold(init, move |p| {
        let f = distr.sample(&mut thread_rng());
        *p = f.ref_call(*p);
        Some((f, *p))
    })
}

pub fn run_slice<'a, F>(rules: &'a [F], init: Point) -> impl Iterator<Item = (&'a F, Point)> + 'a
where
    &'a F: CallRef<Point, Returns = Point>,
{
    run_move(
        rand::distributions::Slice::new(rules).expect("empty slice"),
        init,
    )
}

pub fn run_par<'a, D, F, I>(
    distr: &'a D,
    inits: I,
) -> impl ParallelIterator<Item = impl Iterator<Item = (F, Point)> + 'a> + 'a
where
    D: Distribution<F> + Sync,
    F: CallRef<Point, Returns = Point> + Sync,
    I: ParallelIterator<Item = Point> + 'a,
{
    inits.map(|init| run(distr, init))
}

pub fn run_move_par<'a, D, F, I>(
    distr: D,
    inits: I,
) -> impl ParallelIterator<Item = impl Iterator<Item = (F, Point)> + 'a> + 'a
where
    D: Distribution<F> + Sync + Send + Clone + 'a,
    F: CallRef<Point, Returns = Point> + Sync + 'a,
    I: ParallelIterator<Item = Point> + 'a,
{
    inits.map(move |init| run_move(distr.clone(), init))
}

pub fn run_slice_par<'a, F, I>(
    rules: &'a [F],
    inits: I,
) -> impl ParallelIterator<Item = impl Iterator<Item = (&'a F, Point)> + 'a> + 'a
where
    F: Sync,
    &'a F: CallRef<Point, Returns = Point>,
    I: ParallelIterator<Item = Point> + 'a,
{
    inits.map(|init| run_slice(rules, init))
}
