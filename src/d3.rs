use crate::Point as Point2;

#[derive(PartialEq, PartialOrd, Clone, Copy, Default, Debug)]
pub struct Point3 {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl Point3 {
    pub const fn new(x: f64, y: f64, z: f64) -> Point3 {
        Point3 { x, y, z }
    }

    pub fn rotate_x(self, θ: f64) -> Point3 {
        Point3 {
            x: self.x,
            y: θ.cos() * self.y - θ.sin() * self.z,
            z: θ.sin() * self.y + θ.cos() * self.z,
        }
    }

    pub fn rotate_y(self, θ: f64) -> Point3 {
        Point3 {
            x: θ.cos() * self.x - θ.sin() * self.z,
            y: self.y,
            z: θ.sin() * self.x + θ.cos() * self.z,
        }
    }

    pub fn rotate_z(self, θ: f64) -> Point3 {
        Point3 {
            x: θ.cos() * self.x - θ.sin() * self.y,
            y: θ.sin() * self.x - θ.sin() * self.y,
            z: self.z,
        }
    }
}

#[derive(PartialEq, PartialOrd, Clone, Copy, Default, Debug)]
pub struct Projection {
    pub distance: f64,
    pub fov: f64,
}

impl Projection {
    pub fn project(&self, p: Point3) -> Point2 {
        Point2 {
            x: (self.distance * (self.fov / 2.).tan() * p.x) / (self.distance + p.z),
            y: (self.distance * (self.fov / 2.).tan() * p.y) / (self.distance + p.z),
        }
    }
}
