pub trait Grammar {
    type Sym: Copy;

    fn translate(s: Self::Sym) -> Vec<Self::Sym> {
        let mut ret = Vec::new();
        Self::extend(&mut ret, s);
        ret
    }

    fn extend(out: &mut Vec<Self::Sym>, s: Self::Sym) {
        out.extend(Self::translate(s));
    }

    fn expand(init: &[Self::Sym], times: usize) -> Vec<Self::Sym> {
        // Use double-buffering to minimize allocations.
        let mut ret = Vec::from(init);
        let mut buf = Vec::new();
        for _ in 0..times {
            std::mem::swap(&mut buf, &mut ret);
            for s in &buf {
                Self::extend(&mut ret, *s);
            }
            buf.clear();
        }
        ret
    }
}
