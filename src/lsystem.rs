use crate::Linear;
use crate::Point;
use crate::Vector;
use itertools::Itertools;

pub trait Symbol: Sized + Copy {
    fn expand(self) -> Vec<Self> {
        Self::expands(std::iter::once(self))
    }

    fn expand_into(self, out: &mut Vec<Self>) {
        out.extend(self.expand().into_iter())
    }

    fn expands<I: Iterator<Item = Self>>(iter: I) -> Vec<Self> {
        iter.flat_map(Self::expand).collect_vec()
    }

    fn expands_into<I: Iterator<Item = Self>>(iter: I, out: &mut Vec<Self>) {
        for s in iter {
            s.expand_into(out);
        }
    }

    fn expands_n<I: Iterator<Item = Self>>(iter: I, times: usize) -> Vec<Self> {
        let mut buf1 = iter.collect_vec();
        let mut buf2 = Vec::new();
        for _ in 0..times {
            buf2.reserve(buf1.len());
            Self::expands_into(buf1.iter().copied(), &mut buf2);
            buf1.clear();
            std::mem::swap(&mut buf1, &mut buf2);
        }
        buf1
    }
}

#[derive(PartialEq, PartialOrd, Clone, Copy, Default, Debug)]
pub struct State {
    pub pos: Point,
    pub angle: f64,
}

#[derive(PartialEq, PartialOrd, Clone, Copy, Default, Debug)]
pub struct Params {
    pub translation: Vector,
    pub angle: f64,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Debug)]
pub enum Sym {
    Forward,
    Plus,
    Minus,
    Push,
    Pop,
    Nop,
}
use Sym::*;

impl Params {
    pub fn run<'a, T: Into<Sym>, I: Iterator<Item = T> + 'a>(
        &'a self,
        syms: I,
        state: State,
    ) -> impl Iterator<Item = (Point, Point)> + 'a {
        syms.scan(vec![state], |states, s| {
            let st = states.last_mut()?;
            let p = st.pos;
            Some(match s.into() {
                Forward => {
                    st.pos += Linear::rotation(st.angle) * self.translation;
                    Some((p, st.pos))
                }
                Plus => {
                    st.angle += self.angle;
                    None
                }
                Minus => {
                    st.angle -= self.angle;
                    None
                }
                Push => {
                    // Drop the borrow so that we can modify the stack.
                    let st = *st;
                    states.push(st);
                    None
                }
                Pop => {
                    // This can't fail, since we already borrowed the top of the
                    // stack at the beginning.
                    states.pop().unwrap();
                    None
                }
                Nop => None,
            })
        })
        .flatten()
    }
}
