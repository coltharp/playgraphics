use crate::draw;
use crate::paint;
use crate::Affine;
use crate::Color;
use crate::Paint;
use crate::Point;
use image::DynamicImage;
use image::ImageBuffer;
use image::Rgb;

pub struct Graphics<'a> {
    buf: &'a mut [u32],
    width: usize,
    height: usize,
    pub color: Color,
    pub paint: Paint,
    pub transform: Affine,
}

impl<'a> Graphics<'a> {
    pub fn new(buf: &'a mut [u32], width: usize, height: usize) -> Self {
        if height * width != buf.len() {
            panic!();
        }
        Self {
            buf,
            width,
            height,
            color: Color::default(),
            paint: paint::color(Color::default()),
            transform: Affine::identity(),
        }
    }

    #[inline]
    fn logical_to_raster_transform(&self) -> Affine {
        Affine::reflection_y(self.height as f64 / 2.) * self.transform
    }

    #[inline]
    fn logical_to_raster(t: Affine, p: Point) -> (i32, i32) {
        let p = t * p;
        assert!(
            i32::MIN as f64 <= p.x && p.x <= i32::MAX as f64,
            "logical x out of bounds"
        );
        assert!(
            i32::MIN as f64 <= p.y && p.y <= i32::MAX as f64,
            "logical y out of bounds"
        );
        (p.x.round() as i32, p.y.round() as i32)
    }

    #[inline]
    fn raster_to_logical(height: usize, (x, y): (i32, i32)) -> Point {
        Point {
            x: x as f64,
            y: height as f64 - y as f64 - 1.,
        }
    }

    #[inline]
    fn raster_to_physical(&self, (x, y): (i32, i32)) -> Option<(usize, usize)> {
        let x = x.try_into().ok()?;
        let y = y.try_into().ok()?;
        if x >= self.width || y >= self.height {
            return None;
        }
        Some((x, y))
    }

    pub fn clear(&mut self) {
        self.buf.fill(self.color.to_0rgb());
    }

    pub fn paint_clear(&mut self) {
        for i in 0..self.width {
            for j in 0..self.height {
                self.paint((i as i32, j as i32));
            }
        }
    }

    fn draw(&mut self, u: (i32, i32)) {
        if let Some((x, y)) = self.raster_to_physical(u) {
            let ix = y * self.width + x;
            self.buf[ix] = self.color.to_0rgb();
        }
    }

    fn paint(&mut self, u: (i32, i32)) {
        if let Some((x, y)) = self.raster_to_physical(u) {
            let p = Self::raster_to_logical(self.height, u);
            let ix = y * self.width + x;
            self.buf[ix] = (self.paint)(p, Color::from_0rgb(self.buf[ix])).to_0rgb();
        }
    }

    pub fn pixel(&self, p: Point) -> Option<Color> {
        let t = self.logical_to_raster_transform();
        let (x, y) = self.raster_to_physical(Self::logical_to_raster(t, p))?;
        let ix = y * self.width + x;
        Some(Color::from_0rgb(self.buf[ix]))
    }

    pub fn draw_point(&mut self, p: Point) {
        let t = self.logical_to_raster_transform();
        self.draw(Self::logical_to_raster(t, p));
    }

    pub fn paint_point(&mut self, p: Point) {
        let t = self.logical_to_raster_transform();
        self.paint(Self::logical_to_raster(t, p));
    }

    pub fn draw_line(&mut self, p: Point, q: Point) {
        let t = self.logical_to_raster_transform();
        let p = Self::logical_to_raster(t, p);
        let q = Self::logical_to_raster(t, q);
        draw::line(|u| self.draw(u), p, q);
    }

    pub fn paint_line(&mut self, p: Point, q: Point) {
        let t = self.logical_to_raster_transform();
        let p = Self::logical_to_raster(t, p);
        let q = Self::logical_to_raster(t, q);
        draw::line(|u| self.paint(u), p, q);
    }

    pub fn draw_polygon(&mut self, points: &[Point]) {
        let t = self.logical_to_raster_transform();
        draw::polygon(
            |u| self.draw(u),
            points.iter().map(|&p| Self::logical_to_raster(t, p)),
        );
    }

    pub fn paint_polygon(&mut self, points: &[Point]) {
        let t = self.logical_to_raster_transform();
        draw::polygon(
            |u| self.paint(u),
            points.iter().map(|&p| Self::logical_to_raster(t, p)),
        );
    }

    pub fn fill_polygon(&mut self, points: &[Point]) {
        let t = self.logical_to_raster_transform();
        draw::fill_polygon(
            |u| self.draw(u),
            points.iter().map(|&p| Self::logical_to_raster(t, p)),
        )
    }

    pub fn paint_fill_polygon(&mut self, points: &[Point]) {
        let t = self.logical_to_raster_transform();
        draw::fill_polygon(
            |u| self.paint(u),
            points.iter().map(|&p| Self::logical_to_raster(t, p)),
        )
    }

    pub fn screenshot(&self) -> DynamicImage {
        let width = self.width;
        let height = self.height;
        let buf = ImageBuffer::from_fn(width as u32, height as u32, |x, y| {
            let pixel = self.buf[y as usize * width + x as usize];
            let r = (pixel >> 16) as u8;
            let g = (pixel >> 8) as u8;
            let b = pixel as u8;
            Rgb([r, g, b])
        });
        DynamicImage::ImageRgb8(buf)
    }
}
