/// Low-level drawing routines.
use itertools::Itertools;

/// Draw a line between two points.
pub fn line<F>(mut f: F, (x0, y0): (i32, i32), (x1, y1): (i32, i32))
where
    F: FnMut((i32, i32)),
{
    // Bresenham's algorithm.  Source: Wikipedia.
    //
    // This is a variant that detects the error dynamically in a way
    // that lets it works for all octants simultaneously.  You might
    // think that it would be more efficient to execute a different
    // variant for each octant with a single up-front check to determine
    // which variant to run; however, microbenchmarks reveal that that
    // approach is actually (slightly) *less* efficient!
    let dx = (x1 - x0).abs();
    let sx = if x0 < x1 { 1 } else { -1 };
    let dy = -(y1 - y0).abs();
    let sy = if y0 < y1 { 1 } else { -1 };

    let mut x = x0;
    let mut y = y0;
    let mut error = dx + dy;
    loop {
        f((x, y));
        if x == x1 && y == y1 {
            break;
        }
        let e2 = 2 * error;
        if e2 >= dy {
            if x == x1 {
                break;
            }
            error += dy;
            x += sx;
        }
        if e2 <= dx {
            if y == y1 {
                break;
            }
            error += dx;
            y += sy;
        }
    }
}

pub fn polygon<F, U>(mut f: F, points: U)
where
    F: FnMut((i32, i32)),
    U: ExactSizeIterator<Item = (i32, i32)> + Clone,
{
    for (p, q) in points.circular_tuple_windows() {
        line(&mut f, p, q);
    }
}

pub fn fill_polygon<F, U>(mut f: F, mut points: U)
where
    F: FnMut((i32, i32)),
    U: ExactSizeIterator<Item = (i32, i32)> + Clone,
{
    // Sources:
    //
    // "A New Polygon Based Algorithm for Filling Regions"
    //
    // (it’s about a different algorithm, but it mentions this one for
    // comparison)
    //
    // https://www.cs.uic.edu/~jbell/CourseNotes/ComputerGraphics/PolygonFilling.html

    const MSG: &str =
        "fill_polygon: got fewer points than expected (your ExactSizeIterator is bugged!)";
    match points.len() {
        0 => return,
        1 => {
            f(points.next().expect(MSG));
            return;
        }
        2 => {
            line(f, points.next().expect(MSG), points.next().expect(MSG));
            return;
        }
        _ => (),
    }

    let (y_min, y_max) = points
        .clone()
        .map(|p| p.1)
        .minmax()
        .into_option()
        .expect(MSG);

    // edge_groups will be a list of edge groups where each group consists of
    // edges that have the same initial (low) y-value, sorted in order of
    // initial y-value.
    //
    // (In fact, it will be sorted in *reverse* order; i.e., the lowest y-value
    // will come last.  The reason for this is that we're going to pop groups
    // off the end one-by-one, and we want to go in order of low y-value.)
    let mut edge_groups = points
        // Iterate over pairs of adjacent points; i.e., edges.
        .circular_tuple_windows()
        // Remove horizontal edges.
        .filter(|(p, q)| p.1 != q.1)
        // Make sure the point with the low y-value always occurs first.
        .map(|(p, q)| if p.1 < q.1 { (p, q) } else { (q, p) })
        // Sort and group edges by low y-value.
        .sorted_by(|p, q| q.0 .1.cmp(&p.0 .1))
        .group_by(|p| p.0 .1)
        .into_iter()
        // For each group of lines with the same initial (low) y-value:
        .map(|(y, group)| {
            (
                y,
                group
                    // Compute the slope of each line.
                    .map(|((x0, y0), (x1, y1))| (y1, x0, ((x1 - x0) as f64) / ((y1 - y0) as f64)))
                    // Sort by x-value.
                    .sorted_by_key(|u| u.1)
                    .map(|(y, x, m)| (y, x as f64, m)),
            )
        })
        .collect_vec();

    let mut active = Vec::<(i32, f64, f64)>::new();
    for y in y_min..=y_max {
        // Drop any edge whose final y-value lies on the current scanline.
        active.retain(|u| u.0 != y);
        // If the initial y-value of the next edge group is equal to the y-value
        // of the current scanline, pop that group and add its edges to the
        // active set.
        if edge_groups.last().map_or(false, |g| g.0 == y) {
            active.extend(edge_groups.pop().unwrap().1);
        }
        // Sort edges by x-value.  NOTE The standard sorting algorithm might be
        // less-than-ideal here, since the list will usually be already sorted
        // or mostly-sorted.  If we were really going for absolute maximum
        // performance, we would pull in a library with a sorting algorithm
        // optimized for mostly-sorted data.
        active.sort_by(|u, v| u.1.partial_cmp(&v.1).unwrap());
        // Draw scanlines using the parity rule.
        for (u, v) in active.iter().tuple_windows().step_by(2) {
            let x0 = u.1.round() as i32;
            let x1 = v.1.round() as i32;
            for x in x0..=x1 {
                f((x, y));
            }
        }
        // Increase the x-value of each active edge by its slope.
        for u in &mut active {
            u.1 += u.2;
        }
    }
}
