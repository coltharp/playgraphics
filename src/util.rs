use crate::Affine;
use crate::Point;
use crate::Vector;
use core_extensions::callable::*;
use rand::prelude::*;
use std::cmp::Ordering;

/// Given a figure (specified as a set of points) and viewport dimensions,
/// compute an affine transformation that, when applied to the figure, will
/// center the figure and scale it to fit within the viewport.
pub fn auto_place<I>(mut width: f64, mut height: f64, points: I) -> Affine
where
    I: Iterator<Item = Point>,
{
    width -= 1.;
    height -= 1.;
    // Compute the bounding box of the figure.
    let (lx, ly, hx, hy) = points.fold(
        (
            f64::INFINITY,
            f64::INFINITY,
            f64::NEG_INFINITY,
            f64::NEG_INFINITY,
        ),
        |(lx, ly, hx, hy), p| (lx.min(p.x), ly.min(p.y), hx.max(p.x), hy.max(p.y)),
    );
    // Compute the viewport center and the figure’s current center.
    let viewport_center = Point { x: 0., y: 0. }.midpoint(Point {
        x: width,
        y: height,
    });
    let center = Point { x: lx, y: ly }.midpoint(Point { x: hx, y: hy });

    // Compute the scaling factor.
    let scale_x = width / (hx - lx);
    let scale_y = height / (hy - ly);
    let scale = scale_x.min(scale_y);
    Affine::dilation(viewport_center, scale) * Affine::translation(viewport_center - center)
}

pub fn center_of_mass(points: &[Point]) -> Point {
    Point::origin()
        + (points.iter().map(|p| p - Point::origin()).sum::<Vector>() / points.len() as f64)
}

pub fn standard_deviation(points: &[Point]) -> (Point, f64) {
    let c = center_of_mass(points);
    let σ =
        (points.iter().map(|p| (p - c).norm_squared()).sum::<f64>() / points.len() as f64).sqrt();
    (c, σ)
}

struct PartialConst<T, F> {
    value: T,
    function: F,
}

impl<T, Params, F> CallInto<Params> for PartialConst<T, F>
where
    F: CallInto<(T, Params)>,
{
    type Returns = F::Returns;

    #[inline]
    fn into_call_(self, params: Params) -> F::Returns {
        self.function.into_call((self.value, params))
    }
}

impl<Params, T, F> CallMut<Params> for PartialConst<T, F>
where
    T: Clone,
    F: CallMut<(T, Params)>,
{
    #[inline]
    fn mut_call_(&mut self, params: Params) -> F::Returns {
        self.function.mut_call((self.value.clone(), params))
    }
}

impl<Params, T, F> CallRef<Params> for PartialConst<T, F>
where
    T: Clone,
    F: CallRef<(T, Params)>,
{
    #[inline]
    fn ref_call_(&self, params: Params) -> F::Returns {
        self.function.ref_call((self.value.clone(), params))
    }
}

impl<'a, T, Params, F> CallInto<Params> for &'a PartialConst<T, F>
where
    T: Clone,
    F: CallRef<(T, Params)>,
{
    type Returns = F::Returns;

    #[inline]
    fn into_call_(self, params: Params) -> F::Returns {
        self.function.ref_call((self.value.clone(), params))
    }
}

impl<'a, Params, T, F> CallMut<Params> for &'a PartialConst<T, F>
where
    T: Clone,
    F: CallRef<(T, Params)>,
{
    #[inline]
    fn mut_call_(&mut self, params: Params) -> F::Returns {
        (*self).mut_call(params)
    }
}

impl<'a, Params, T, F> CallRef<Params> for &'a PartialConst<T, F>
where
    T: Clone,
    F: CallRef<(T, Params)>,
{
    #[inline]
    fn ref_call_(&self, params: Params) -> F::Returns {
        (*self).ref_call(params)
    }
}

pub struct Carrier<T, F> {
    pub value: T,
    pub function: F,
}

impl<Params, T, F> CallInto<Params> for Carrier<T, F>
where
    F: CallInto<Params>,
{
    type Returns = F::Returns;

    #[inline]
    fn into_call_(self, params: Params) -> F::Returns {
        self.function.into_call(params)
    }
}

impl<Params, T, F> CallMut<Params> for Carrier<T, F>
where
    F: CallMut<Params>,
{
    #[inline]
    fn mut_call_(&mut self, params: Params) -> F::Returns {
        self.function.mut_call(params)
    }
}

impl<Params, T, F> CallRef<Params> for Carrier<T, F>
where
    F: CallRef<Params>,
{
    #[inline]
    fn ref_call_(&self, params: Params) -> F::Returns {
        self.function.ref_call(params)
    }
}

impl<Params, T, F> CallInto<Params> for &Carrier<T, F>
where
    F: CallRef<Params>,
{
    type Returns = F::Returns;

    #[inline]
    fn into_call_(self, params: Params) -> F::Returns {
        self.function.ref_call(params)
    }
}

impl<Params, T, F> CallMut<Params> for &Carrier<T, F>
where
    F: CallRef<Params>,
{
    #[inline]
    fn mut_call_(&mut self, params: Params) -> F::Returns {
        (*self).mut_call(params)
    }
}

impl<Params, T, F> CallRef<Params> for &Carrier<T, F>
where
    F: CallRef<Params>,
{
    #[inline]
    fn ref_call_(&self, params: Params) -> F::Returns {
        (*self).ref_call(params)
    }
}

#[inline]
pub fn carry_indices<I: Iterator>(iter: I) -> impl Iterator<Item = Carrier<usize, I::Item>> {
    iter.enumerate()
        .map(|(value, function)| Carrier { value, function })
}

#[derive(Eq, PartialEq, Ord, PartialOrd, Clone, Copy, Hash, Default, Debug)]
struct ZipDistributions<D1, D2> {
    d1: D1,
    d2: D2,
}

impl<T1, D1, T2, D2> Distribution<(T1, T2)> for ZipDistributions<D1, D2>
where
    D1: Distribution<T1>,
    D2: Distribution<T2>,
{
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> (T1, T2) {
        (self.d1.sample(rng), self.d2.sample(rng))
    }
}

pub fn zip_distributions<T1, D1, T2, D2>(d1: D1, d2: D2) -> impl Distribution<(T1, T2)>
where
    D1: Distribution<T1>,
    D2: Distribution<T2>,
{
    ZipDistributions { d1, d2 }
}

pub struct Keyed<K, T>(pub K, pub T);

impl<K, T> PartialEq for Keyed<K, T>
where
    K: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl<K, T> Eq for Keyed<K, T> where K: Eq {}

impl<K, T> PartialOrd for Keyed<K, T>
where
    K: PartialOrd,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.0.partial_cmp(&other.0)
    }
}

impl<K, T> Ord for Keyed<K, T>
where
    K: Ord,
{
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}
