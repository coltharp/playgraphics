use std::time::{Duration, Instant};

pub trait Time {
    fn update(&mut self) -> Duration;
}

impl Time for Instant {
    fn update(&mut self) -> Duration {
        let ret = self.elapsed();
        *self = Instant::now();
        ret
    }
}

/// Virtual timer that always reports the same amount of time passed between
/// frames.
pub struct VirtualTime {
    pub duration: Duration,
}

impl Time for VirtualTime {
    fn update(&mut self) -> Duration {
        self.duration
    }
}
