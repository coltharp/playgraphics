use crate::Graphics;
use crate::Point;
use crate::Vector;
use image::DynamicImage;
use image::ImageBuffer;
use image::Rgb;
use minifb::Window;
use minifb::WindowOptions;
use num::FromPrimitive;
use num::ToPrimitive;
use num_derive::FromPrimitive;
use num_derive::ToPrimitive;
use serde::{Deserialize, Serialize};
use std::time::Duration;
use std::time::Instant;

#[derive(
    PartialEq,
    PartialOrd,
    Eq,
    Ord,
    Clone,
    Copy,
    Debug,
    Serialize,
    Deserialize,
    FromPrimitive,
    ToPrimitive,
)]
pub enum MouseButton {
    Left = 0,
    Middle = 1,
    Right = 2,
}
use MouseButton::*;

impl MouseButton {
    fn to_minifb(self) -> minifb::MouseButton {
        match self {
            Left => minifb::MouseButton::Left,
            Middle => minifb::MouseButton::Middle,
            Right => minifb::MouseButton::Right,
        }
    }
}

#[derive(
    PartialEq,
    PartialOrd,
    Eq,
    Ord,
    Clone,
    Copy,
    Debug,
    Serialize,
    Deserialize,
    FromPrimitive,
    ToPrimitive,
)]
pub enum Modifier {
    Ctrl,
    Alt,
    Shift,
}
use Modifier::*;

impl Modifier {
    fn from_minifb(key: minifb::Key) -> Option<Modifier> {
        match key {
            minifb::Key::LeftCtrl | minifb::Key::RightCtrl => Some(Ctrl),
            minifb::Key::LeftAlt | minifb::Key::RightAlt => Some(Alt),
            minifb::Key::LeftShift | minifb::Key::RightShift => Some(Shift),
            _ => None,
        }
    }
}

#[derive(PartialEq, PartialOrd, Eq, Ord, Clone, Copy, Default, Debug, Serialize, Deserialize)]
pub enum UpDownState {
    #[default]
    Up,
    Down,
}
use UpDownState::*;

impl UpDownState {
    fn from_down(down: bool) -> Self {
        if down {
            Down
        } else {
            Up
        }
    }

    fn to_mouse_event(self, button: MouseButton) -> Event {
        match self {
            Up => MouseUp(button),
            Down => MouseDown(button),
        }
    }
}

fn translate_key(key: minifb::Key, mods: &[UpDownState; 3]) -> Option<char> {
    if mods[Shift.to_usize().unwrap()] == Up {
        match key {
            minifb::Key::A => Some('a'),
            minifb::Key::B => Some('b'),
            minifb::Key::C => Some('c'),
            minifb::Key::D => Some('d'),
            minifb::Key::E => Some('e'),
            minifb::Key::F => Some('f'),
            minifb::Key::G => Some('g'),
            minifb::Key::H => Some('h'),
            minifb::Key::I => Some('i'),
            minifb::Key::J => Some('j'),
            minifb::Key::K => Some('k'),
            minifb::Key::L => Some('l'),
            minifb::Key::M => Some('m'),
            minifb::Key::N => Some('n'),
            minifb::Key::O => Some('o'),
            minifb::Key::P => Some('p'),
            minifb::Key::Q => Some('q'),
            minifb::Key::R => Some('r'),
            minifb::Key::S => Some('s'),
            minifb::Key::T => Some('t'),
            minifb::Key::U => Some('u'),
            minifb::Key::V => Some('v'),
            minifb::Key::W => Some('w'),
            minifb::Key::X => Some('x'),
            minifb::Key::Y => Some('y'),
            minifb::Key::Z => Some('z'),
            minifb::Key::Apostrophe => Some('\''),
            minifb::Key::Backquote => Some('`'),
            minifb::Key::Backslash => Some('\\'),
            minifb::Key::Comma => Some(','),
            minifb::Key::Equal => Some('='),
            minifb::Key::LeftBracket => Some('['),
            minifb::Key::Minus => Some('-'),
            minifb::Key::Period => Some('.'),
            minifb::Key::RightBracket => Some(']'),
            minifb::Key::Semicolon => Some(';'),
            minifb::Key::Slash => Some('/'),
            minifb::Key::Space => Some(' '),
            minifb::Key::Tab => Some('\t'),
            _ => None,
        }
    } else {
        match key {
            minifb::Key::A => Some('A'),
            minifb::Key::B => Some('B'),
            minifb::Key::C => Some('C'),
            minifb::Key::D => Some('D'),
            minifb::Key::E => Some('E'),
            minifb::Key::F => Some('F'),
            minifb::Key::G => Some('G'),
            minifb::Key::H => Some('H'),
            minifb::Key::I => Some('I'),
            minifb::Key::J => Some('J'),
            minifb::Key::K => Some('K'),
            minifb::Key::L => Some('L'),
            minifb::Key::M => Some('M'),
            minifb::Key::N => Some('N'),
            minifb::Key::O => Some('O'),
            minifb::Key::P => Some('P'),
            minifb::Key::Q => Some('Q'),
            minifb::Key::R => Some('R'),
            minifb::Key::S => Some('S'),
            minifb::Key::T => Some('T'),
            minifb::Key::U => Some('U'),
            minifb::Key::V => Some('V'),
            minifb::Key::W => Some('W'),
            minifb::Key::X => Some('X'),
            minifb::Key::Y => Some('Y'),
            minifb::Key::Z => Some('Z'),
            minifb::Key::Apostrophe => Some('"'),
            minifb::Key::Backquote => Some('~'),
            minifb::Key::Backslash => Some('|'),
            minifb::Key::Comma => Some('<'),
            minifb::Key::Equal => Some('+'),
            minifb::Key::LeftBracket => Some('{'),
            minifb::Key::Minus => Some('_'),
            minifb::Key::Period => Some('>'),
            minifb::Key::RightBracket => Some('}'),
            minifb::Key::Semicolon => Some(':'),
            minifb::Key::Slash => Some('?'),
            minifb::Key::Space => Some(' '),
            minifb::Key::Tab => Some('\t'),
            _ => None,
        }
    }
}

#[derive(PartialEq, PartialOrd, Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Event {
    MouseMove(Point),
    MouseDown(MouseButton),
    MouseUp(MouseButton),
    KeyDown([UpDownState; 3], char),
    KeyUp([UpDownState; 3], char),
    Scroll(Vector),
}
use Event::*;

#[derive(Debug)]
pub struct System {
    window: Window,
    buf: Vec<u32>,
    // Remaining fields are the state of the previous frame, used for detecting
    // events.
    was_active: bool,
    mouse_pos: Point,
    mouse_button_states: [UpDownState; 3],
    modifier_states: [UpDownState; 3],
}

impl System {
    pub fn new(title: &str, width: usize, height: usize) -> Self {
        let window =
            Window::new(title, width, height, WindowOptions::default()).unwrap_or_else(|err| {
                match err {
                    minifb::Error::WindowCreate(msg) => panic!("{}", msg),
                    _ => unreachable!(),
                }
            });
        let buf = vec![0; height * width];
        let mut ret = Self {
            window,
            buf,
            was_active: bool::default(),
            mouse_pos: Point::default(),
            mouse_button_states: <[UpDownState; 3]>::default(),
            modifier_states: <[UpDownState; 3]>::default(),
        };
        // Update once to initialize our values.  Also, on Darwin, the image
        // doesn't display properly if we haven't called update() once.  This is
        // probably a minifb bug.
        ret.update();
        ret
    }

    pub fn size(&self) -> (usize, usize) {
        self.window.get_size()
    }

    pub fn width(&self) -> usize {
        self.size().0
    }

    pub fn height(&self) -> usize {
        self.size().1
    }

    pub fn pre_update(&mut self) {
        self.was_active = self.window.is_active();
        self.mouse_pos = self.mouse_pos();
        for i in 0..3 {
            self.mouse_button_states[i] = UpDownState::from_down(
                self.window
                    .get_mouse_down(MouseButton::from_usize(i).unwrap().to_minifb()),
            );
        }
    }

    pub fn update(&mut self) -> Vec<Event> {
        self.pre_update();
        self.window.update();
        if !self.was_active && self.window.is_active() {
            self.do_render();
        }
        self.post_update()
    }

    fn do_render(&mut self) {
        let (width, height) = self.window.get_size();
        self.window
            .update_with_buffer(&self.buf, width, height)
            .unwrap_or_else(|err| match err {
                minifb::Error::UpdateFailed(msg) => panic!("{}", msg),
                _ => unreachable!(),
            });
    }

    pub fn render(&mut self) -> Vec<Event> {
        self.pre_update();
        self.do_render();
        self.post_update()
    }

    pub fn post_update(&mut self) -> Vec<Event> {
        if !self.window.is_open() {
            std::process::exit(0);
        }
        let mut ret = Vec::new();
        if self.mouse_pos() != self.mouse_pos {
            ret.push(MouseMove(self.mouse_pos()));
        }
        for i in 0..3 {
            let button = MouseButton::from_usize(i).unwrap();
            let state = self.mouse_button_state(MouseButton::from_usize(i).unwrap());
            if self.mouse_button_states[i] != state {
                ret.push(state.to_mouse_event(button));
            }
        }
        if let Some((x, y)) = self.window.get_scroll_wheel() {
            ret.push(Scroll(Vector::new(x as f64, y as f64)));
        }
        // Update modifier states.
        for key in self.window.get_keys_pressed(minifb::KeyRepeat::No) {
            if let Some(md) = Modifier::from_minifb(key) {
                self.modifier_states[md.to_usize().unwrap()] = Down
            }
        }
        for key in self.window.get_keys_released() {
            if let Some(md) = Modifier::from_minifb(key) {
                self.modifier_states[md.to_usize().unwrap()] = Up
            }
        }
        // Process "normal" key presses.
        for key in self.window.get_keys_pressed(minifb::KeyRepeat::No) {
            if let Some(key) = translate_key(key, &self.modifier_states) {
                ret.push(KeyDown(self.modifier_states, key));
            }
        }
        for key in self.window.get_keys_released() {
            if let Some(key) = translate_key(key, &self.modifier_states) {
                ret.push(KeyUp(self.modifier_states, key));
            }
        }
        ret
    }

    fn graphics(&mut self) -> Graphics {
        let (width, height) = self.window.get_size();
        Graphics::new(&mut self.buf, width, height)
    }

    pub fn draw<F>(&mut self, f: F)
    where
        F: FnOnce(&mut Graphics),
    {
        let mut g = self.graphics();
        f(&mut g);
    }

    pub fn draw_and_render<F>(&mut self, f: F) -> Vec<Event>
    where
        F: FnOnce(&mut Graphics),
    {
        self.draw(f);
        self.render()
    }

    fn mouse_button_state(&self, button: MouseButton) -> UpDownState {
        UpDownState::from_down(self.window.get_mouse_down(button.to_minifb()))
    }

    pub fn mouse_pos(&self) -> Point {
        let (x, y) = self
            .window
            .get_mouse_pos(minifb::MouseMode::Pass)
            .unwrap_or_else(|| unreachable!());
        let (_, height) = self.window.get_size();
        let x = x as f64;
        let y = height as f64 - 1. - y as f64;
        Point { x, y }
    }

    pub fn wait_click(&mut self, button: MouseButton) -> Point {
        loop {
            let events = self.update();
            if events.contains(&MouseDown(button)) {
                break;
            }
        }
        self.mouse_pos()
    }

    pub fn wait(&mut self, dur: Duration) {
        let timer = Instant::now();
        while timer.elapsed() < dur {
            self.update();
        }
    }

    pub fn park(&mut self) -> ! {
        loop {
            self.update();
        }
    }

    pub fn screenshot(&self) -> DynamicImage {
        let width = self.width();
        let height = self.height();
        let buf = ImageBuffer::from_fn(width as u32, height as u32, |x, y| {
            let pixel = self.buf[y as usize * width + x as usize];
            let r = (pixel >> 16) as u8;
            let g = (pixel >> 8) as u8;
            let b = pixel as u8;
            Rgb([r, g, b])
        });
        DynamicImage::ImageRgb8(buf)
    }
}
