use itertools::Itertools;
use rand::prelude::*;

use criterion::{criterion_group, criterion_main, Criterion};
use playgraphics::draw;

pub fn polygon_alt_1<F, U>(mut f: F, mut points: U)
where
    F: FnMut((i32, i32)),
    U: Iterator<Item = (i32, i32)>,
{
    let first = match points.next() {
        None => return,
        Some(p) => p,
    };
    let mut p = first;
    for q in points {
        draw::line(&mut f, p, q);
        p = q;
    }
    draw::line(&mut f, p, first);
}

pub fn polygon_alt_2<F>(mut f: F, points: &[(i32, i32)])
where
    F: FnMut((i32, i32)),
{
    for (&p, &q) in points.iter().circular_tuple_windows() {
        draw::line(&mut f, p, q);
    }
}

pub fn bench_polygon(c: &mut Criterion) {
    let width = 800;
    let height = 800;
    let max_points = 64;

    let mut rng = thread_rng();
    let mut pixels = vec![0; height * width];
    let mut f = |(x, y)| {
        pixels[y as usize * width + x as usize] = 1;
    };

    c.bench_function("polygon", |b| {
        b.iter(|| {
            let n = rng.gen_range(0..=max_points);
            let points = (0..=n)
                .map(|_| {
                    (
                        rng.gen_range(0..width as i32),
                        rng.gen_range(0..height as i32),
                    )
                })
                .collect_vec();
            draw::polygon(&mut f, points.iter().copied());
        })
    });
    c.bench_function("polygon_alt_1", |b| {
        b.iter(|| {
            let n = rng.gen_range(0..=max_points);
            let points = (0..=n)
                .map(|_| {
                    (
                        rng.gen_range(0..width as i32),
                        rng.gen_range(0..height as i32),
                    )
                })
                .collect_vec();
            polygon_alt_1(&mut f, points.iter().copied());
        })
    });
    c.bench_function("polygon_alt_2", |b| {
        b.iter(|| {
            let n = rng.gen_range(0..=max_points);
            let points = (0..=n)
                .map(|_| {
                    (
                        rng.gen_range(0..width as i32),
                        rng.gen_range(0..height as i32),
                    )
                })
                .collect_vec();
            polygon_alt_2(&mut f, &points);
        })
    });
}

criterion_group!(benches, bench_polygon);
criterion_main!(benches);
